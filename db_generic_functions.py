# -*- coding: utf-8 -*-
import pandas as pd

# Quelques fonctions génériques
def import_param_conn(path_conn):
    '''Fonction qui renvoie les paramètres de connexion à la base, lue depuis un fichier csv au format suivant :
    database;user;password;port
    presteaux;user_name;user_password;5432'''

    # on importe le csv avec les paramètres
    conn_param = pd.read_csv(path_conn, sep=';')
    return conn_param


def transform_string(s):
    '''Fonction qui transforme un string donné en un string interprétable lors d'une requête'''

    # on ajoute les '\' qui vont bien
    s = '\'' + s + '\''
    return  s


# une fonction qui remplit la table 'param_potamologique'
def fill_tab_param_potamo(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source):
    '''Fonction qui remplit la table param_potamologique et retourne l'id du param
    out: id param'''

    # remplissage de la table param_potamologique
    cursor.execute("""INSERT INTO param_potamologique (nom_param, unite, famille, methode_calc, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s);
                    """, [param_nom, param_unite, param_famille, param_methode_calc, param_id_source])
    conn.commit()

    # on récupère l'id du paramètre que l'on vient de créer
    cursor.execute("""SELECT id_param FROM param_potamologique
                    WHERE nom_param = %s;
                    """, [param_nom])
    id_param = cursor.fetchone()[0]
    return id_param

# une fonction qui remplit la table 'param_terrestre'
def fill_tab_param_terrestre(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source):
    '''Fonction qui remplit la table param_terrestre et retourne l'id du param
    out: id param'''

    # remplissage de la table param_terrestre
    cursor.execute("""INSERT INTO param_terrestre (nom_param, unite, famille, methode_calc, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s);
                    """, [param_nom, param_unite, param_famille, param_methode_calc, param_id_source])
    conn.commit()

    # on récupère l'id du paramètre que l'on vient de créer
    cursor.execute("""SELECT id_param FROM param_terrestre
                    WHERE nom_param = %s;
                    """, [param_nom])
    id_param = cursor.fetchone()[0]
    return id_param

def getFeatures(gdf):
    """Function to parse features from GeoDataFrame in such a manner that rasterio wants them"""
    import json
    return [json.loads(gdf.to_json())['features'][0]['geometry']]

# une fonction qui remplit la table 'param_littoral'
def fill_tab_param_littoral(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source):
    '''Fonction qui remplit la table param_littoral et retourne l'id du param
    out: id param'''

    # remplissage de la table param_littoral
    cursor.execute("""INSERT INTO param_littoral (nom_param, unite, famille, methode_calc, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s);
                    """, [param_nom, param_unite, param_famille, param_methode_calc, param_id_source])
    conn.commit()

    # on récupère l'id du paramètre que l'on vient de créer
    cursor.execute("""SELECT id_param FROM param_littoral
                    WHERE nom_param = %s;
                    """, [param_nom])
    id_param = cursor.fetchone()[0]
    return id_param

# une fonction qui remplit la table 'param_indicateur_littoral'
def fill_tab_param_indicateur_littoral(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source):
    '''Fonction qui remplit la table param_indicateur_littoral et retourne l'id du param
    out: id param'''

    # remplissage de la table param_indicateur_littoral
    cursor.execute("""INSERT INTO param_indicateur_littoral (nom_param, unite, famille, methode_calc, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s);
                    """, [param_nom, param_unite, param_famille, param_methode_calc, param_id_source])
    conn.commit()

    # on récupère l'id du paramètre que l'on vient de créer
    cursor.execute("""SELECT id_param FROM param_indicateur_littoral
                    WHERE nom_param = %s;
                    """, [param_nom])
    id_param = cursor.fetchone()[0]
    return id_param