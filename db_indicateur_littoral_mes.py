# -*- coding: utf-8 -*-
# calcul des stats zonale des MES journalières pour chaque MECT : médiane, coefficient interquartile et p90, remplissage des tables 'param_indicateur_littoral' et 'indicateur_littoral'
import psycopg2
import geopandas as gpd
import rasterio
from rasterstats import zonal_stats
import numpy as np
import wget
from sh import gunzip
from datetime import datetime, timedelta
import os
from ftplib import FTP
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# l'URL du ftp avec les images
urlMes = 'ftp://ftp.ifremer.fr/ifremer/sextant_data/SATCOAST/atlantic/MES/'

# la période à analyser
startDate_str = '2006-12-07'
endDate_str = '2006-12-31'
validite = 'periode'

# le chemin vers le répertoire de travail
pathWork = '/home/passy/SIG/Odatis/chl_a_jour/'

# les paramètres du paramètre
param_nom = ['MES_sat_mediane', 'MES_sat_coeff_iq', 'MES_sats_p90', 'MES_sat_nb_pix']
param_unite = 'g/m3'
param_famille = 'biologie'
param_methode_calc = 'db_indicateur_littoral_mes.py'
param_id_source = 39

# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_indicateur_littoral' ################
# on vérifie si les paramètres existent déjà ou pas
for param in param_nom:
    cursor.execute("""SELECT id_param FROM param_indicateur_littoral
                    WHERE nom_param = %s;
                    """, [param])
    rsl = cursor.fetchall()
    if len(rsl) == 0:
        id_param = fill_tab_param_indicateur_littoral(conn, cursor, param, param_unite, param_famille, param_methode_calc, param_id_source)


################ calcul des stats de MES et remplissage de la table 'indicateur_littoral' ################
now = datetime.now()
print("Start : " + str(now))
# on charge la couche des mect dans un geodf
sql = "SELECT * FROM mect;"
df_mect = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

# on transforme les dates en objet date
date_inf = datetime.strptime(startDate_str, "%Y-%m-%d")
date_sup = datetime.strptime(endDate_str, "%Y-%m-%d")

# on parcourt tous les jours de la période à traiter
d = date_inf
while d <= date_sup:
    # on récupère la date du jour pour l'insertion dans la table
    d_inf = d
    d_sup = d + timedelta(days=1)

    # on récupère l'année, le mois et le jour
    yy = d.year
    mm = d.month
    dd = d.day

    # on récupère l'image de Chl a du jour
    if mm < 10:
        mm = '0' + str(mm)
    if dd < 10:
        dd = '0' + str(dd)
    #print(urlChla + str(d.year) + '/' + str(yy) + str(mm) + str(dd) + '.tif.gz')

    # on vérifie qu'il y a bien une image pour le jour donné
    ftp = FTP('ftp.ifremer.fr')
    ftp.login()
    ftp.cwd('ifremer/sextant_data/SATCOAST/atlantic/MES/' + str(d.year))
    # si il y a bien une image on la télécharge
    if str(yy) + str(mm) + str(dd) + '.tif.gz' in ftp.nlst():
        wget.download(urlMes + str(d.year) + '/' + str(yy) + str(mm) + str(dd) + '.tif.gz', out=pathWork)
    else:
        d = d + timedelta(days=1)
        continue

    # on décompresse l'image
    # si une image porte déjà ce nom, on la supprime
    if os.path.isfile(pathWork + str(yy) + str(mm) + str(dd) + '.tif') is True:
        os.remove(pathWork + str(yy) + str(mm) + str(dd) + '.tif')
    gunzip(pathWork + str(yy) + str(mm) + str(dd) + '.tif.gz')
    im = pathWork + str(yy) + str(mm) + str(dd) + '.tif'

    # on charge le raster de l'image satellite de la chl a
    im_mes = rasterio.open(im)
    # on récupère les paramètres géographiques
    transform = im_mes.transform
    # on transforme ce raster en array
    array_mes = im_mes.read(1)
    # on met les pixels inrérieurs à 0 à -999 qui est la valeur standard des no data pour zonal stats
    array_mes = np.where(array_mes < 0, -999, array_mes)

    # on reprojette la couche des mect selon le système de l'image sat
    df_mect = df_mect.to_crs(im_mes.crs)

    # on parcourt les mect une par une
    for m in range(0, len(df_mect)):
        # on récupère l'id de la mect
        id_mect = int(df_mect['id_mect'][m])
        #print('mect ' + str(id_mec))
        # on extrait la mect en cours
        current_mect = df_mect.iloc[m:m + 1]
        # on calcule les stats zonales : médiane, Q25, Q75, p90 et count
        zs = zonal_stats(current_mect, array_mes, affine=transform,
                         stats=['median', 'percentile_25', 'percentile_75', 'percentile_90', 'count'])
        # on vérifie que le count soit supérieur à 4, i.e. qu'il y ait au moins 4 pixels de chl a sous la mect en cours
        if zs[0]['count'] <= 4:
            continue
        # on calcule le coefficient interquartile
        mes_iq = (zs[0]['percentile_75'] - zs[0]['percentile_25']) / zs[0]['median']

        # on insère les valeurs dans la table 'indicateur_littoral'
        #  on parcourt les param
        for param in param_nom:
            #  on récupère l'id du param en cours
            cursor.execute("""SELECT id_param FROM param_indicateur_littoral
                            WHERE nom_param = %s;
                            """, [param])
            id_param = cursor.fetchone()
            #print(id_param)
            # on aiguille selon le paramètre
            if param == 'MES_sat_mediane':
                val = zs[0]['median']
            elif param == 'MES_sat_coeff_iq':
                val = mes_iq
            elif param == 'MES_sat_p90':
                val = zs[0]['percentile_90']
            elif param == 'MES_sat_nb_pix':
                val = zs[0]['count']

            #  on remplit la table indicateur_littoral pour le paramètre de pente correspondant
            cursor.execute("""INSERT INTO indicateur_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                            VALUES
                              (%s, %s, %s, %s, %s, %s, %s);
                            """, [id_mect, id_param, val, d_inf, d_sup, validite, param_id_source])
            conn.commit()

    # we delete the downloaded image
    os.remove(im)

    d = d + timedelta(days=1)
    #break

now = datetime.now()
print('This is the End for the MES, at : ' + str(now))