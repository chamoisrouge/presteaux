# -*- coding: utf-8 -*-
# valeurs de phytoplancton issues de la base REPHY SEANOE, si il y a plusieurs données pour une même MECT et un même jour on prend la valeur moyenne
# remplissage des tables 'param_indicateur_littoral' et 'indicateur_littoral'
import geopandas as gpd
import pandas as pd
import psycopg2
import math
from datetime import datetime

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin vers le csv du Rephy
#path_csv = '/home/passy/SIG/Rephy_Seanoe/REPHY_Phyto_Manche_Atlantique_1987-2018.csv'
path_csv = '/home/passy/SIG/Rephy_Seanoe/REPHY_Phyto_Med_1987-2018.csv'

# le chemin vers le geopkg des points Rephy joints aux MECT
path_points_rephy = '/home/passy/SIG/Rephy_Seanoe/rephy_mnemonique_mect_L93.gpkg'

# les paramètres du paramètre
# id_source Rephy Seanoe
param_famille = 'biologie'
param_methode_calc = 'db_indicateur_littoral_rephy_seanoe_phyto'
param_id_source = 41

# le seuil d'occurrences pour garder un taxon
n_seuil_taxon = 1000

# le seuil d'occurrences pour garder un param
n_seuil_param = 500


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()
########################################################################################################################
# on importe les points Rephy qui contiennent les code MECT
points_rephy = gpd.read_file(path_points_rephy)

# on importe le csv du Rephy
rephy_data = pd.read_csv(path_csv, sep=';', encoding='ISO-8859-1')

# on regroupe les données par taxons afin de ne garder que les taxons qui ont été prélevés au moins le nombre de fois souhaité par l'utilisateur
taxons_seuils = rephy_data.groupby(['Résultat : Taxon : code SANDRE'], as_index=False)['Résultat : Valeur de la mesure'].count()
taxons_seuils = taxons_seuils[taxons_seuils['Résultat : Valeur de la mesure'] >= n_seuil_taxon]
# on renomme la colonne qui contient les comptes
taxons_seuils = taxons_seuils.rename(columns={'Résultat : Valeur de la mesure': 'compte'})

# on joint ce compte au df des données originales afin de ne sélectionner que les données en nombre suffisant
rephy_data_f = pd.merge(rephy_data, taxons_seuils, on='Résultat : Taxon : code SANDRE', how='right')

# on filtre pour ne garder que les données bonnes ou non qualifiées
rephy_data_f = rephy_data_f[rephy_data_f['Passage : Niveau de qualité'].isin(['Non qualifié', 'Bon'])]
rephy_data_f = rephy_data_f[rephy_data_f['Prélèvement : Niveau de qualité'].isin(['Non qualifié', 'Bon'])]
rephy_data_f = rephy_data_f[rephy_data_f['Echantillon : Niveau de qualité'].isin(['Non qualifié', 'Bon'])]
rephy_data_f = rephy_data_f[rephy_data_f['Résultat : Niveau de qualité'].isin(['Non qualifié', 'Bon'])]
# on filtre les profondeurs à 999 et à NaN
rephy_data_f = rephy_data_f[rephy_data_f['Prélèvement : Immersion'] != 999]
rephy_data_f = rephy_data_f[rephy_data_f['Prélèvement : Immersion'].notnull()]
# on filtre les lignes sans résultat
rephy_data_f = rephy_data_f[rephy_data_f['Résultat : Valeur de la mesure'].notnull()]

# on associe une classe de profondeur de prélévement ('0_1m', '1_5m') (Prélèvement : Immersion)
# on repère la profondeur max
max_prof = rephy_data_f['Prélèvement : Immersion'].max()

# on définit une fonction qui associe à chaque profondeur une classe de profondeur (de 5m en 5m)
def create_prof(row):
    if row['Prélèvement : Immersion'] <= 1:
        classe_prof = '0_1m'
    else:
        p = row['Prélèvement : Immersion']
        if math.floor(p/5) * 5 == 0:
            classe_prof = '1_' + str(math.floor(p/5) * 5 + 5) + 'm'
        else:
            classe_prof = str(math.floor(p/5) * 5) + '_' + str(math.floor(p/5) * 5 + 5) + 'm'
    return classe_prof

# on applique la fonction d'association de classes de profondeur
rephy_data_f['classe_prof'] = rephy_data_f.apply(create_prof, axis=1)

# on créé une colonne 'nom_param' et on la remplit
rephy_data_f['nom_param'] = rephy_data_f.apply(lambda row: 'Rephy_Aphia_' + str(row['Résultat : Nom du taxon: AphiaID']) +\
                                               '_' + row['Résultat : Nom du taxon : Source AphiaID'] +\
                                               '_' + row['Résultat : Nom du taxon'] +\
                                             '_prof_' + row['classe_prof'] , axis=1)

# on regroupe par nom_param et unité
param_rephy = rephy_data_f.groupby(['nom_param', 'Résultat : Symbole unité de mesure associé au quadruplet'], as_index=False)['Résultat : Valeur de la mesure'].count()
# on ne conserve que les params avec un nombre d'occurrences supérieurs au seuil fixé par l'utilisateur
param_rephy = param_rephy[param_rephy['Résultat : Valeur de la mesure'] >= n_seuil_param]
# on supprime la colonne de compte
param_rephy = param_rephy.drop(['Résultat : Valeur de la mesure'], axis=1)

# ajout des param dans la table 'param_indicateur_littoral'
# on parcourt le df des param rephy
for i in range(0, len(param_rephy['nom_param'])):
    # on récupère le nom et l'unité
    param_nom = param_rephy['nom_param'].iloc[i]
    param_unite = param_rephy['Résultat : Symbole unité de mesure associé au quadruplet'].iloc[i]
    # remplissage de la table param_indicateur_littoral après vérification que le paramètre n'existe pas déjà
    # on regarde si le paramètre existe déjà
    cursor.execute("""SELECT * FROM param_indicateur_littoral
                    WHERE nom_param = %s AND id_source = %s
                      ;
                    """, [param_nom, param_id_source])
    rsl = cursor.fetchall()

    # si le paramère n'existe pas on l'ajoute
    if len(rsl) == 0:
        cursor.execute("""INSERT INTO param_indicateur_littoral (nom_param, unite, famille, methode_calc, id_source)
                        VALUES
                          (%s, %s, %s, %s, %s)
                          ;
                        """, [param_nom, param_unite, param_famille, param_methode_calc, param_id_source])
        conn.commit()

# on joint ce compte au df des données originales afin de ne sélectionner que les données en nombre suffisant
rephy_data_f = pd.merge(rephy_data_f, param_rephy, on='nom_param', how='right')

# on joint le code des MECT aux données
rephy_data_mect = pd.merge(rephy_data_f, points_rephy, left_on='Lieu de surveillance : Mnémonique', right_on='Mnémonique', how='inner')

# on regroupe les données par MECT, nom_param, date de passage afin de ne prendre que la moyenne des valeurs si on a plus d'une mesure par jour et par MECT
rephy_data_to_insert = rephy_data_mect.groupby(['code_mect', 'nom_param', 'Passage : Date'], as_index=False)['Résultat : Valeur de la mesure'].mean()

# insertion des données de mesures dans la base
# on parcourt le df des données
for i in range(0, len(rephy_data_to_insert['nom_param'])):
    # on récupère le code de la mect
    code_mect = rephy_data_to_insert['code_mect'].iloc[i]
    # on récupère l'id de la mect correspondant à ce code
    cursor.execute("""SELECT id_mect FROM mect
                WHERE code_mect = %s;
                """, [code_mect])
    id_mect = cursor.fetchone()

    # on récupère l'id_param
    nom_param = rephy_data_to_insert['nom_param'].iloc[i]
    cursor.execute("""SELECT id_param FROM param_indicateur_littoral
            WHERE nom_param = %s;
            """, [nom_param])
    id_param = cursor.fetchone()

    # on construit la date comme il faut
    y = rephy_data_to_insert['Passage : Date'].iloc[i][6:]
    m = rephy_data_to_insert['Passage : Date'].iloc[i][3:5]
    d = rephy_data_to_insert['Passage : Date'].iloc[i][0:2]
    date_start = datetime.strptime(y + '-' + m + '-' + d, "%Y-%m-%d")

    # on récupère la valeur de mesure
    val = rephy_data_to_insert['Résultat : Valeur de la mesure'].iloc[i]

    # on insère le tout dans la table 'indicateur_littoral'
    cursor.execute("""INSERT INTO indicateur_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s, %s, %s)
                      ON CONFLICT (id_mect, id_param, date_inf, date_sup, id_source)
                      DO NOTHING;
                    """, [id_mect, id_param, val, date_start, date_start, 'periode', param_id_source])
    conn.commit()
    #break

print('This is the End for the Rephy Seanoe phyto!')