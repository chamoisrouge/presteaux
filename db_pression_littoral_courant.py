# -*- coding: utf-8 -*-
# calcul des stats des vitesses de courant (médiane, coefficient interquartile et p90) pour chaque MECT, remplissage des tables 'param_littoral' et 'pression_littoral'
import pandas as pd
import geopandas as gpd
import psycopg2
from datetime import datetime
from db_generic_functions import *
import rasterio
import numpy as np
from rasterstats import zonal_stats

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin verst le raster des vitesses de courant
path_vit = '/home/passy/SIG/geolittoral/N_courant_v95_iso_L_092014/N_courant_v95_iso_S.tif'

# la date associée à la couche des vitesses de courant
date_vit = '10-07-2014'
validite = 'mise_a_jour'

# les paramètres du paramètre
param_nom = ['vitesse_mediane', 'vitesse_coeff_iq', 'vitesse_p90', 'vitesse_nb_pix']
param_unite = 'm/s'
param_famille = 'hydrographie'
param_methode_calc = 'db_pression_littoral_courant.py'
param_id_source = 37


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_littoral' ################
# on vérifie si le paramètre existe déjà ou pas
for param in param_nom:
    cursor.execute("""SELECT id_param FROM param_littoral
                    WHERE nom_param = %s and id_source = %s;
                    """, [param, param_id_source])
    rsl = cursor.fetchall()
    # si le param n'existe pas on le créé
    if len(rsl) == 0:
        # si le param est 'vitesse_nb_pix', on change l'unité par 'compte'
        if param == 'vitesse_nb_pix':
            param_unite_compte = 'compte'
            id_param = fill_tab_param_littoral(conn, cursor, param, param_unite_compte, param_famille, param_methode_calc, param_id_source)
        else:
            id_param = fill_tab_param_littoral(conn, cursor, param, param_unite, param_famille, param_methode_calc, param_id_source)
    else:
        id_param = rsl[0]

################ calcul des stats zonales des vitesses et remplissage de la table 'pression_littoral' ################
# on construit les dates
date_inf = datetime.strptime(date_vit, "%d-%m-%Y")
date_sup = date_inf

# on charge le raster des vitesses de courant
vit_courant = rasterio.open(path_vit)
# on récupère ses paramètres de projection
transform = vit_courant.transform
# on récupère ses valeurs dans un array
array_vit_courant = vit_courant.read(1)
# on met les pixels inrérieurs à 0 à -999 qui est la valeur standard des no data pour zonal stats
array_vit_courant = np.where(array_vit_courant < 0, -999, array_vit_courant)

# on charge la couche des mect dans un geodf
sql = "SELECT * FROM mect;"
df_mect = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

# on parcourt les mect
for m in range(0, len(df_mect)):
    # on récupère l'id de la mect
    id_mect = int(df_mect['id_mect'][m])

    # print('mect ' + str(id_mec))
    # on extrait la mect en cours
    current_mect = df_mect.iloc[m:m + 1]

    # on compte le nombre de pixels de vitesses de courant recouverts par la mect en cours
    zs = zonal_stats(current_mect, array_vit_courant, affine=transform, stats=['count'])
    nb_pix = zs[0]['count']

    # si il n'y a pas de pixels de vitesses on passe à la mect suivante
    if nb_pix == 0:
        continue

    # on calcule les stats zonales : médiane, Q25, Q75, p90 et count
    zs = zonal_stats(current_mect, array_vit_courant, affine=transform,
                     stats=['median', 'percentile_25', 'percentile_75', 'percentile_90'])
    # on calcule le coefficient interquartile
    vit_iq = (zs[0]['percentile_75'] - zs[0]['percentile_25']) / zs[0]['median']

    #  on insère les valeurs dans la table 'pression_littoral'
    #  on parcourt les param
    for param in param_nom:
        #  on récupère l'id du param en cours
        cursor.execute("""SELECT id_param FROM param_littoral
                        WHERE nom_param = %s AND id_source = %s;
                        """, [param, param_id_source])
        id_param = cursor.fetchone()
        # on aiguille selon le paramètre
        if param == 'vitesse_mediane':
            val = zs[0]['median']
        elif param == 'vitesse_coeff_iq':
            val = vit_iq
        elif param == 'vitesse_p90':
            val = zs[0]['percentile_90']
        elif param == 'vitesse_nb_pix':
            val = nb_pix

        #  on remplit la table pression_littoral pour le paramètre de pente correspondant
        cursor.execute("""INSERT INTO pression_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                        VALUES
                          (%s, %s, %s, %s, %s, %s, %s);
                        """, [id_mect, id_param, val, date_inf, date_sup, validite, param_id_source])
        conn.commit()

print('This is the End for the Courants!')