# -*- coding: utf-8 -*-
# calcul des centroïdes de chaque MECT, remplissage des tables 'param_littoral' et 'pression_littoral'
import pandas as pd
import psycopg2
from datetime import datetime
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# la date associée à la couche servant à calculer les centroïdes (jj-mm-yyyy, en string)
date_ctr = '01-01-2016'
validite = 'mise_a_jour'

# les paramètres du paramètre
param_list = ['centroide_x', 'centroide_y']
param_unite = 'm'
param_famille = 'géographie'
param_methode_calc = 'db_pression_littorale_centroide.py'
param_id_source = 13


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_littoral' ################
# on vérifie si le paramètre existe déjà ou pas
for param_nom in param_list:
    cursor.execute("""SELECT id_param FROM param_littoral
                    WHERE nom_param = %s;
                    """, [param_nom])
    rsl = cursor.fetchall()
    if len(rsl) == 0:
        id_param = fill_tab_param_littoral(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source)
    else:
        id_param = rsl[0]

################ calcul des centroides et remplissage de la table 'pression_littoral' ################
# on sélectionne les cood X et Y des centroïdes de chaque MECT
sql = '''with ctr as(
select id_mect, ST_Centroid(geom_2154) centroid from mect)
select id_mect, st_x(centroid) ctr_X, st_y(centroid) ctr_Y
from ctr'''
df_ctr = pd.read_sql(sql, conn)

# on construit les dates
date_inf = datetime.strptime(date_ctr, "%d-%m-%Y")
date_sup = date_inf

# insertion des valeurs dans la base
# on parcourt le df des centroïdes
for i in range(len(df_ctr['id_mect'])):
    # on récupère l'id_mect
    id_mect = int(df_ctr['id_mect'].iloc[i])

    # on récupère les X et Y
    ctr_x = df_ctr['ctr_x'].iloc[i]
    ctr_y = df_ctr['ctr_y'].iloc[i]

    # on parcourt les params
    for param in param_list:
        #  on récupère l'id du param en cours
        cursor.execute("""SELECT id_param FROM param_littoral
                        WHERE nom_param = %s;
                        """, [param])
        id_param = cursor.fetchone()
        # on aiguille selon le paramètre
        if param == 'centroide_x':
            val = ctr_x
        elif param == 'centroide_y':
            val = ctr_y

        #  on remplit la table pression_littoral pour le paramètre d'altitude correspondant
        cursor.execute("""INSERT INTO pression_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                        VALUES
                          (%s, %s, %s, %s, %s, %s, %s);
                        """, [id_mect, id_param, val, date_inf, date_sup, validite, param_id_source])
        conn.commit()


print('This is the End for the Centroides!')