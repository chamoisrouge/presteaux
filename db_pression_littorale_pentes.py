# -*- coding: utf-8 -*-
# calcul des stats des pentes bathy pour chaque MECT (médiane et coeef interquartile, remplissage des tables 'param_littoral' et 'pression_littoral'
import pandas as pd
import geopandas as gpd
import psycopg2
from datetime import datetime
from db_generic_functions import *
import rasterio
from rasterstats import zonal_stats
import numpy as np

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin vers les pentes bathy Atl, GDL, Corse
path_pentes_atl = '/home/passy/SIG/Bathy/MNT_FACADE_ATLANTIQUE_HOMONIM_PBMA/DONNEES/MNT_ATL100m_HOMONIM_PBMA_ZNEG_slope_L93.tif'
path_pentes_gdl = '/home/passy/SIG/Bathy/MNT_FACADE_GDL-CA_HOMONIM_PBMA/DONNEES/MNT_MED100m_GDL-CA_HOMONIM_PBMA_ZNEG_slope_L93.tif'
path_pentes_corse = '/home/passy/SIG/Bathy/MNT_FACADE_CORSE_HOMONIM_PBMA_BAG/DONNEES/MNT_MED100m_CORSE_HOMONIM_PBMA_ZNEG_slope_L93.tif'

# la date associée à la couche servant à calculer les superficies (jj-mm-yyyy, en string), ici il s'agit de la bathy du SHOM
date_pentes = '01-01-2015'
validite = 'mise_a_jour'

# les paramètres du paramètre
param_list = ['pente_mect_mediane', 'pente_mect_coef_IQ']
param_unite = 'degrés'
param_famille = 'topo bathymétrie'
param_methode_calc = 'db_pression_littorale_pentes.py'
param_id_source = 16


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_littoral' ################
# on vérifie si le paramètre existe déjà ou pas
for param_nom in param_list:
    cursor.execute("""SELECT id_param FROM param_littoral
                    WHERE nom_param = %s;
                    """, [param_nom])
    rsl = cursor.fetchall()
    if len(rsl) == 0:
        id_param = fill_tab_param_littoral(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source)
    else:
        id_param = rsl[0]

# on construit un param qui correspondra au nombre de pixels de bathy disponibles sous une PBMA donnée
cursor.execute("""SELECT id_param FROM param_littoral
                WHERE nom_param = %s AND id_source = %s;
                """, ['nb_pixels', param_id_source])
rsl = cursor.fetchall()
if len(rsl) == 0:
    id_param_nb_pix = fill_tab_param_littoral(conn, cursor, 'nb_pixels', 'compte', param_famille, param_methode_calc, param_id_source)
else:
    id_param_nb_pix = rsl[0]

################ calcul des stats des pentes des MECT et remplissage de la table 'pression_littoral' ################
# on construit les dates
date_inf = datetime.strptime(date_pentes, "%d-%m-%Y")
date_sup = date_inf

# on charge les pentes bathymétriques et on configure les no data à -999
# Manche Atlantique
pentes_atl = rasterio.open(path_pentes_atl)
# on récupère ses paramètres de projection
transform_atl = pentes_atl.transform
# on récupère ses valeurs dans un array
array_pentes_atl = pentes_atl.read(1)
array_pentes_atl = np.where(array_pentes_atl == -9999.0, -999, array_pentes_atl)

# Golfe du Lyon
pentes_gdl = rasterio.open(path_pentes_gdl)
# on récupère ses paramètres de projection
transform_gdl = pentes_gdl.transform
# on récupère ses valeurs dans un array
array_pentes_gdl = pentes_gdl.read(1)
array_pentes_gdl = np.where(array_pentes_gdl == -9999.0, -999, array_pentes_gdl)

# Corse
pentes_corse = rasterio.open(path_pentes_corse)
# on récupère ses paramètres de projection
transform_corse = pentes_corse.transform
# on récupère ses valeurs dans un array
array_pentes_corse = pentes_corse.read(1)
array_pentes_corse = np.where(array_pentes_corse == -9999.0, -999, array_pentes_corse)

# on charge la table des mect en geodf
sql = "SELECT * FROM mect;"
df_mect = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

# on liste les mect
cursor.execute("""SELECT id_mect FROM mect; """)
lst_id_mect = cursor.fetchall()

# on parcourt les mect
for id_mect in lst_id_mect:
    # on récupère le code de la mect
    cursor.execute("""SELECT code_mect FROM mect
    WHERE id_mect = %s; 
    """, [id_mect])
    code_mect = cursor.fetchone()

    # on sélectionne la mect correspondante et on la sauve en geodf
    sql = "SELECT * FROM mect WHERE id_mect = %s;" % id_mect
    df_mect = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

    # on repère sur quelle façade se trouve la mect en fonction de son code et on sélectionne la bathy correspondante
    if code_mect[0][:3] == 'FRD':
        array_pentes = array_pentes_gdl
        transform = transform_gdl
    elif code_mect[0][:3] == 'FRE':
        array_pentes = array_pentes_corse
        transform = transform_corse
    else:
        array_pentes = array_pentes_atl
        transform = transform_atl

    # on compte le nombre de pixels de bathy qui sont recouverts par la PBMA en question
    zs = zonal_stats(df_mect, array_pentes, affine=transform, stats=['count'])
    nb_pix = zs[0]['count']

    # si il n'y a pas de pixels de bathy on passe à la MECT suivante
    if nb_pix == 0:
        continue

    # on calcule la pente médiane et les Q25 et Q75 de la mect en cours via les zonal stats
    zs = zonal_stats(df_mect, array_pentes, affine=transform, stats=['median', 'percentile_25', 'percentile_75'])
    # on récupère la médiane
    pentes_med = zs[0]['median']
    # on calcule le coefficient interquartile
    pentes_iq_rel = zs[0]['percentile_75'] - zs[0]['percentile_25']

    #  on insère le nombre de pixels de bathy dans la table
    cursor.execute("""INSERT INTO pression_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s, %s, %s);
                    """, [id_mect, id_param_nb_pix, nb_pix, date_inf, date_sup, validite, param_id_source])
    conn.commit()

    # on insère les stats des pentes dans la table 'pression_littoral'
    # on parcourt les param
    for param in param_list:
        #  on récupère l'id du param en cours
        cursor.execute("""SELECT id_param FROM param_littoral
                        WHERE nom_param = %s;
                        """, [param])
        id_param = cursor.fetchone()
        # on aiguille selon le paramètre
        if param == 'pente_mect_mediane':
            val = pentes_med
        elif param == 'pente_mect_coef_IQ':
            val = pentes_iq_rel

        #  on remplit la table pression_littoral pour le paramètre d'altitude correspondant
        cursor.execute("""INSERT INTO pression_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                        VALUES
                          (%s, %s, %s, %s, %s, %s, %s);
                        """, [id_mect, id_param, val, date_inf, date_sup, validite, param_id_source])
        conn.commit()

    #break


print('This is the End for the Pentes des PBMA!')