# -*- coding: utf-8 -*-
# calcul des stats des profondeurs des PBMA (médiane, coef interquartile) pour chaque MECT, remplissage des tables 'param_littoral' et 'pression_littoral'
import pandas as pd
import geopandas as gpd
import psycopg2
from datetime import datetime
from db_generic_functions import *
import rasterio
from rasterstats import zonal_stats

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin vers la couche des superficies des ZIT des MEC
path_mec = '/home/passy/SIG/masses_eau/ZoneIntertidale_MasseDEauCotiere.gpkg'
# le chemin vers la couche des superficies des ZIT des MET
path_met = '/home/passy/SIG/masses_eau/ZoneIntertidale_MasseDEauTransition.gpkg'

# le chemin verst les bathy Atl, GDL, Corse
path_bathy_atl = '/home/passy/SIG/Bathy/MNT_FACADE_ATLANTIQUE_HOMONIM_PBMA/DONNEES/MNT_ATL100m_HOMONIM_PBMA_ZNEG_L93.tif'
path_bathy_gdl = '/home/passy/SIG/Bathy/MNT_FACADE_GDL-CA_HOMONIM_PBMA/DONNEES/MNT_MED100m_GDL-CA_HOMONIM_PBMA_ZNEG_L93.tif'
path_bathy_corse = '/home/passy/SIG/Bathy/MNT_FACADE_CORSE_HOMONIM_PBMA_BAG/DONNEES/MNT_MED100m_CORSE_HOMONIM_PBMA_ZNEG_L93.tif'

# la date associée à la couche servant à calculer les superficies (jj-mm-yyyy, en string), ici il s'agit de la bathy du SHOM
date_pbma = '01-01-2015'
validite = 'mise_a_jour'

# les paramètres du paramètre
param_list = ['profondeur_pbma_mediane', 'profondeur_pbma_coef_IQ']
param_unite = 'm'
param_famille = 'hydrographie'
param_methode_calc = 'db_pression_littorale_prof.py'
param_id_source = 15


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_littoral' ################
# on vérifie si le paramètre existe déjà ou pas
for param_nom in param_list:
    cursor.execute("""SELECT id_param FROM param_littoral
                    WHERE nom_param = %s;
                    """, [param_nom])
    rsl = cursor.fetchall()
    if len(rsl) == 0:
        id_param = fill_tab_param_littoral(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source)
    else:
        id_param = rsl[0]

# on construit un param qui correspondra au nombre de pixels de bathy disponibles sous une PBMA donnée
cursor.execute("""SELECT id_param FROM param_littoral
                WHERE nom_param = %s AND id_source = %s;
                """, ['nb_pixels', param_id_source])
rsl = cursor.fetchall()
if len(rsl) == 0:
    id_param_nb_pix = fill_tab_param_littoral(conn, cursor, 'nb_pixels', 'compte', param_famille, param_methode_calc, param_id_source)
else:
    id_param_nb_pix = rsl[0]

################ calcul du volume des PBMA et remplissage de la table 'pression_littoral' ################
# on construit les dates
date_inf = datetime.strptime(date_pbma, "%d-%m-%Y")
date_sup = date_inf

# on charge les bathy
# Manche Atlantique
bathy_atl = rasterio.open(path_bathy_atl)
# on récupère ses paramètres de projection
transform_atl = bathy_atl.transform
# on récupère ses valeurs dans un array
array_bathy_atl = bathy_atl.read(1)

# Golfe du Lyon
bathy_gdl = rasterio.open(path_bathy_gdl)
# on récupère ses paramètres de projection
transform_gdl = bathy_gdl.transform
# on récupère ses valeurs dans un array
array_bathy_gdl = bathy_gdl.read(1)

# Corse
bathy_corse = rasterio.open(path_bathy_corse)
# on récupère ses paramètres de projection
transform_corse = bathy_corse.transform
# on récupère ses valeurs dans un array
array_bathy_corse = bathy_corse.read(1)

# on charge les couches des PBMA
pbma_mec = gpd.read_file(path_mec, layer='mec_zones_pelagiques')
pbma_met = gpd.read_file(path_met, layer='met_zones_pelagiques')

# on ne garde que les colonnes 'CdEuMasseD' et 'geometry'
pbma_mec = pbma_mec[['CdEuMasseD', 'geometry']]
pbma_met = pbma_met[['CdEuMasseD', 'geometry']]

# on concatène les MEC et les MET
pbma_mect = pd.concat([pbma_mec, pbma_met])

# on charge la table des mect en geodf
sql = "SELECT * FROM mect;"
df_mect = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

# on liste les mect
cursor.execute("""SELECT id_mect FROM mect; """)
lst_id_mect = cursor.fetchall()

# on parcourt les mect
for id_mect in lst_id_mect:
    # on récupère le code de la mect
    cursor.execute("""SELECT code_mect FROM mect
    WHERE id_mect = %s; 
    """, [id_mect])
    code_mect = cursor.fetchone()

    # on récupère la pbma en cours
    pbma = pbma_mect[pbma_mect['CdEuMasseD'] == code_mect[0]]

    # si la pbma n'existe pas, on passe à la suivante
    if len(pbma) == 0:
        continue

    # on repère sur quelle façade se trouve la mect en fonction de son code et on sélectionne la bathy correspondante
    if code_mect[0][:3] == 'FRD':
        array_bathy = array_bathy_gdl
        transform = transform_gdl
    elif code_mect[0][:3] == 'FRE':
        array_bathy = array_bathy_corse
        transform = transform_corse
    else:
        array_bathy = array_bathy_atl
        transform = transform_atl

    # on compte le nombre de pixels de bathy recouverts par la PBMA en question
    zs = zonal_stats(pbma, array_bathy, affine=transform, stats=['count'])
    nb_pix = zs[0]['count']

    # si il n'y a pas de pixels de bathy on passe à la PBMA suivante
    if nb_pix == 0:
        continue

    # on calcule la profondeur médiane et les Q25 et Q75 de la pbma en cours via les zonal stats
    zs = zonal_stats(pbma, array_bathy, affine=transform, stats=['median', 'percentile_25', 'percentile_75'])
    # on récupère la médiane
    prof_med = zs[0]['median']
    # on calcule le coefficient interquartile
    prof_iq_rel = zs[0]['percentile_75'] - zs[0]['percentile_25']

    #  on insère le nombre de pixels de bathy pour la PBMA dans la table sauf si cette valeur existe déjà
    cursor.execute("""INSERT INTO pression_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                    VALUES (%s, %s, %s, %s, %s, %s, %s)
                    ON CONFLICT (id_mect, id_param, date_inf, date_sup, id_source)
                    DO NOTHING
                    ;
                    """, [id_mect, id_param_nb_pix, nb_pix, date_inf, date_sup, validite, param_id_source])
    conn.commit()

    # on insère les stats des profondeurs dans la table 'pression_littoral'
    # on parcourt les param
    for param in param_list:
        #  on récupère l'id du param en cours
        cursor.execute("""SELECT id_param FROM param_littoral
                        WHERE nom_param = %s;
                        """, [param])
        id_param = cursor.fetchone()
        # on aiguille selon le paramètre
        if param == 'profondeur_pbma_mediane':
            val = prof_med
        elif param == 'profondeur_pbma_coef_IQ':
            val = prof_iq_rel

        #  on remplit la table pression_littoral pour le paramètre d'altitude correspondant
        cursor.execute("""INSERT INTO pression_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                        VALUES
                          (%s, %s, %s, %s, %s, %s, %s);
                        """, [id_mect, id_param, val, date_inf, date_sup, validite, param_id_source])
        conn.commit()


    #break

print('This is the End for the Profondeurs des PBMA!')