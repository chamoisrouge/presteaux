# -*- coding: utf-8 -*-
# calcul de la superficie de chaque classe de sédimentologie pour chaque MECT, remplissage des tables 'param_littoral' et 'pression_littoral'
import pandas as pd
import geopandas as gpd
import psycopg2
from datetime import datetime
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin vers la couche de la sédimentologie côtière
path_sedim = '/home/passy/SIG/sedimento_cotes/natures_fond_G_2016.gpkg'

# la date associée à la couche servant à calculer les superficies (jj-mm-yyyy, en string)
date_sedim = '01-01-2016'
validite = 'mise_a_jour'

# les paramètres du paramètre
# le chemin vers le csv contenant le nom des classes de sédimentologie et les noms des params
path_param = '/home/passy/SIG/sedimento_cotes/param_sedimento.csv'
# le nom du paramètre qui compte la fraction de la MECT couverte par la couche de sédimento
param_couv = 'sedim_couverture'
# le préfixe du paramètre de cet usage du sol
pref_param = 'msedim_'
param_unite = 'km2'
param_famille = 'morphologie'
param_methode_calc = 'db_pression_littorale_sedimento.py'
param_id_source = 18


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_littoral' ################
# on charge le csv contenant le nom des paramètres
param_df = pd.read_csv(path_param, sep=';')

# on vérifie si les paramètres existent déjà ou pas
for param in param_df['nom_param']:
    cursor.execute("""SELECT id_param FROM param_littoral
                    WHERE nom_param = %s;
                    """, [param])
    rsl = cursor.fetchall()
    if len(rsl) == 0:
        id_param = fill_tab_param_littoral(conn, cursor, param, param_unite, param_famille, param_methode_calc, param_id_source)

# on ajoute un paramètre qui stocke la frcation de la MECT couverte par la couche de sédimento
# on vérifie si ce paramètre existe déjà ou pas
cursor.execute("""SELECT id_param FROM param_littoral
                WHERE nom_param = %s and id_source = %s;
                """, [param_couv, param_id_source])
rsl = cursor.fetchall()
if len(rsl) == 0:
    id_param_fract_couv = fill_tab_param_littoral(conn, cursor, param_couv, 'fraction de mect couverte', param_famille, param_methode_calc,
                                        param_id_source)


################ calcul des superficies totales de chaque classe de sédimentologie et remplissage de la table 'pression_littoral' ################
#  construction des champs date_inf et date_sup
date_inf = datetime.strptime(date_sedim, "%d-%m-%Y")
date_sup = date_inf

# on charge la couche de sédimento dans un geodf
sedim = gpd.read_file(path_sedim)

# on supprime les polygones dont le type est 'trait de cote'
sedim = sedim[sedim.DESCRIP != 'trait de cote']

# on charge la table des mect en geodf
sql = "SELECT * FROM mect;"
df_mect = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

# on calcule la superficie de chaque mect
df_mect['sup_mect_km2'] = df_mect['geom_2154'].area / 1000000

# on intersecte les mect et la sédimento
mect_sedim = gpd.overlay(df_mect, sedim, how='intersection')

# on calcule l'aire de chaque polygone d'intersection
mect_sedim['sup_km2'] = mect_sedim['geometry'].area / 1000000

# on regroupe par mect et par type de sédimento
mect_type_sedim = mect_sedim.groupby(['id_mect', 'code_mect', 'sup_mect_km2', 'TYPE_VALEU'], as_index=False)['sup_km2'].sum()

# on insère les valeurs de superficie de chaque classe de sédimentologie
# on parcourt le df des types de sédimento par mect
for i in range(len(mect_type_sedim['id_mect'])):
    # on récupère l'id mect
    id_mect = float(mect_type_sedim.iloc[i]['id_mect'])

    # on récupère l'id param
    nom_param = param_df[param_df['classe'] == mect_type_sedim.iloc[i]['TYPE_VALEU']]['nom_param'].iloc[0]
    cursor.execute("""SELECT id_param FROM param_littoral
                    WHERE nom_param = %s;
                    """, [nom_param])
    id_param = cursor.fetchone()

    # la valeur du paramètre
    val = float(mect_type_sedim.iloc[i]['sup_km2'])

    # on remplit la table pression_littoral
    cursor.execute("""INSERT INTO pression_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s, %s, %s);
                    """, [id_mect, id_param, val, date_inf, date_sup, validite, param_id_source])
    conn.commit()

    #break

# on compte la fraction de mect couverte par la couche de sédimentologie. La fraction peut-être supérieure à 1 car il y a des polygones de sédimento superposés les uns sur les autres...
# on calcule la fraction de la mect couverte par la couche de sédimento
mect_fract_sedim = mect_sedim.groupby(['id_mect', 'code_mect', 'sup_mect_km2'], as_index=False)['sup_km2'].sum()
mect_fract_sedim['fraction_sedim'] = mect_fract_sedim['sup_km2'] / mect_fract_sedim['sup_mect_km2']

# on récupère l'id du param correspondant au paramètre décrivant la fraction couverte
cursor.execute("""SELECT id_param FROM param_littoral
                WHERE nom_param = %s;
                """, [param_couv])
id_param_fract_couv = cursor.fetchone()

# on parcourt le df avec les fractions couvertes
for i in range(len(mect_fract_sedim['id_mect'])):
    # on récupère l'id mect
    id_mect = float(mect_fract_sedim.iloc[i]['id_mect'])

    # on récupère la valeur de fraction
    val_frac = float(mect_fract_sedim.iloc[i]['fraction_sedim'])

    # on remplit la table pression_littoral
    cursor.execute("""INSERT INTO pression_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s, %s, %s);
                    """, [id_mect, id_param_fract_couv, val_frac, date_inf, date_sup, validite, param_id_source])
    conn.commit()

    #break

print('This is the End for the Sédimentologie!')