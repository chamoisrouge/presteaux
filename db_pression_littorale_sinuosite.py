# -*- coding: utf-8 -*-
# calcul de l'edge density (sinuosité) de chaque MECT, remplissage des tables 'param_littoral' et 'pression_littoral'
import geopandas as gpd
import pandas as pd
import psycopg2
from datetime import datetime
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# la date associée à la couche servant à calculer les superficies (jj-mm-yyyy, en string)
date_edge = '01-01-2016'
validite = 'mise_a_jour'

# les paramètres du paramètre
param_nom = 'edge_density_mect'
param_unite = 'm/m2'
param_famille = 'morphologie'
param_methode_calc = 'db_pression_littorale_sinuosite.py'
param_id_source = 13


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_littoral' ################
# on vérifie si le paramètre existe déjà ou pas
cursor.execute("""SELECT id_param FROM param_littoral
                WHERE nom_param = %s;
                """, [param_nom])
rsl = cursor.fetchall()
if len(rsl) == 0:
    id_param = fill_tab_param_littoral(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source)
else:
    id_param = rsl[0]

################ calcul de la edge density et remplissage de la table 'pression_littoral' ################
# on charge la table des mect en geodf
sql = "SELECT * FROM mect;"
df_mect = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

# on calcule l'edge density de chaque mect
df_mect['edge_density'] = df_mect['geom_2154'].length / df_mect['geom_2154'].area

#  construction des champs date_inf et date_sup
date_inf = datetime.strptime(date_edge, "%d-%m-%Y")
date_sup = date_inf

# on parcourt la liste des id mect
for id_mect in df_mect['id_mect']:
    # on récupère l'edge density de la mect courante
    edge_density = float(df_mect[df_mect['id_mect'] == id_mect]['edge_density'])

    # on insère cette valeur dans la table 'pression_littoral'
    cursor.execute("""INSERT INTO pression_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s, %s, %s);
                    """, [id_mect, id_param, edge_density, date_inf, date_sup, validite, param_id_source])
    conn.commit()

    #break



print('This is the End for the Edge Density!')