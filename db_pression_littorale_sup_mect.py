# -*- coding: utf-8 -*-
# calcul de la superficie de chaque MECT, remplissage des tables 'param_littoral' et 'pression_littoral'
import pandas as pd
import geopandas as gpd
import psycopg2
import numpy as np
from datetime import datetime
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# la date associée à la couche servant à calculer les superficies (jj-mm-yyyy, en string)
date_bv = '01-01-2016'
validite = 'mise_a_jour'

# les paramètres du paramètre
param_nom = 'superficie_mect'
param_unite = 'km2'
param_famille = 'hydrographie'
param_methode_calc = 'db_pression_littorale_sup_mect.py'
param_id_source = 13


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_littoral' ################
# on vérifie si le paramètre existe déjà ou pas
cursor.execute("""SELECT id_param FROM param_littoral
                WHERE nom_param = %s;
                """, [param_nom])
rsl = cursor.fetchall()
if len(rsl) == 0:
    id_param = fill_tab_param_littoral(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source)
else:
    id_param = rsl[0]

################ calcul de la superfice et remplissage de la table 'pression_littoral' ################
# on récupère la table des mect sous forme d'un dataframe
df_mect = pd.read_sql_query('SELECT id_mect FROM "mect"', con=conn)

#  construction des champs date_inf et date_sup
date_inf = datetime.strptime(date_bv, "%d-%m-%Y")
date_sup = date_inf

# on parcourt la liste des id mect
for id_mect in df_mect['id_mect']:
    # on sélectionne la superficie de la mect courante
    cursor.execute("""SELECT ST_AREA(geom_2154) FROM mect
                    WHERE id_mect = %s;
                    """ % (id_mect))
    rsl = cursor.fetchone()
    sup_mect = rsl[0] / 1000000

    # on remplit la table pression_littoral pour la superficie de la mect
    cursor.execute("""INSERT INTO pression_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s, %s, %s);
                    """, [id_mect, id_param, sup_mect, date_inf, date_sup, validite, param_id_source])
    conn.commit()

print('This is the End for the Superficie!')