# -*- coding: utf-8 -*-
# calcul de la superficie de PBMA pour chaque MECT, remplissage des tables 'param_littoral' et 'pression_littoral'
import pandas as pd
import geopandas as gpd
import psycopg2
import numpy as np
from datetime import datetime
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin vers la couche des superficies des PBMA des MEC
path_mec = '/home/passy/SIG/masses_eau/ZoneIntertidale_MasseDEauCotiere.gpkg'
# le chemin vers la couche des superficies des PBMA des MET
path_met = '/home/passy/SIG/masses_eau/ZoneIntertidale_MasseDEauTransition.gpkg'
# les MET à exclure
met_exclude = [121, 122, 123, 156, 171, 173, 186, 197]

# la date associée à la couche servant à calculer les superficies (jj-mm-yyyy, en string), ici il s'agit de la bathy du SHOM
date_pbma = '01-01-2015'
validite = 'mise_a_jour'

# les paramètres du paramètre
param_nom = 'superficie_pbma'
param_unite = 'km2'
param_famille = 'hydrographie'
param_methode_calc = 'db_pression_littorale_sup_pbma.py'
param_id_source = 14


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_littoral' ################
# on vérifie si le paramètre existe déjà ou pas
cursor.execute("""SELECT id_param FROM param_littoral
                WHERE nom_param = %s;
                """, [param_nom])
rsl = cursor.fetchall()
if len(rsl) == 0:
    id_param = fill_tab_param_littoral(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source)
else:
    id_param = rsl[0]

################ calcul de la superfice des PBMA et remplissage de la table 'pression_littoral' ################
# on charge les couches des PBMA pour les MEC et les MET
pbma_mec = gpd.read_file(path_mec, layer='mec_zones_pelagiques')
pbma_met = gpd.read_file(path_met, layer='met_zones_pelagiques')

# on concatène les MEC et les MET
pbma_mect = pd.concat([pbma_mec, pbma_met])

# on calcule les aires en km2 de chaque PBMA
pbma_mect['sup_km2'] = pbma_mec['geometry'].area / 1000000

# on charge la table des mect en geodf
sql = "SELECT * FROM mect;"
df_mect = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

# on liste les mect
cursor.execute("""SELECT id_mect FROM mect; """)
lst_id_mect = cursor.fetchall()

# on construit les dates inf et sup
date_inf = datetime.strptime(date_pbma, "%d-%m-%Y")
date_sup = date_inf

# on parcourt les mect
for id_mect in lst_id_mect:
    # si l'id mect figure parmi la liste des met à exclure, on passe
    if id_mect[0] in met_exclude:
        continue
    # on récupère le code de la mect
    cursor.execute("""SELECT code_mect FROM mect
    WHERE id_mect = %s; 
    """, [id_mect])
    code_mect = cursor.fetchone()

    # on récupère la superficie de la PBMA de la mect en cours
    sup_pbma = pbma_mect[pbma_mect['CdEuMasseD'] == code_mect[0]]['sup_km2']

    # si il n'y a pas de PBMA pour la mect considérée on met la superficie à 0
    if len(sup_pbma) == 0:
        sup_pbma = 0
    else:
        sup_pbma = float(sup_pbma)

    #  on remplit la table pression_littoral pour la superficie des PBMA
    cursor.execute("""INSERT INTO pression_littoral (id_mect, id_param, valeur, date_inf, date_sup, validite, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s, %s, %s);
                    """, [id_mect, id_param, sup_pbma, date_inf, date_sup, validite, param_id_source])
    conn.commit()

    #break

print('This is the End for the Superficie des PBMA!')