# -*- coding: utf-8 -*-
# calcul de la densité de drainage pour chaque bassin, remplissage des tables 'param_potamologique' et 'pression_potamologique'
# chaque tronçon du réseau hydrographique doit être associée à un bassin, prétraitement à réaliser avant de faire tourner ce script
import pandas as pd
import geopandas as gpd
import psycopg2
import numpy as np
from datetime import datetime
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin vers la couche du réseau hydrographique
path_hydro = '/home/passy/SIG/masses_eau/TronconElemMasseDEauRiviereExut_FXX.gpkg'
# le nom du 'layer' (si nécessaire, dans le cas d'un Geopackage)
layer = 'TronconElemMasseDEauRiviereExut_FXX'
# le nom du champ contenant de ode du bassin
champ_code_bv = 'exut'

# la date associée à la couche des tronçons hydro (jj-mm-yyyy, en string)
date_hydro = '01-01-2016'

# les paramètres du paramètre
param_nom = 'densite_drainage'
param_unite = 'km/km2'
param_famille = 'hydrographie'
param_methode_calc = 'db_pression_potamo_densite_drainage.py'
param_id_source = 2


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_potamologique' ################
# on vérifie si le paramètre existe déjà ou pas
cursor.execute("""SELECT id_param FROM param_potamologique
                WHERE nom_param = %s;
                """, [param_nom])
rsl = cursor.fetchall()
if len(rsl) == 0:
    id_param = fill_tab_param_potamo(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source)
else:
    id_param = rsl[0]


################ calcul de la densité de drainage et remplissage de la table 'pression_potamologique' ################
# on récupère la couche des tronçons sous forme d'un data frame
hydro = gpd.read_file(path_hydro, layer=layer)

# on récupère la table sous forme d'un dataframe
df_bv = pd.read_sql_query('SELECT id_bassin FROM "bassin"', con=conn)
# on parcourt la liste des id bv
for id_bv in df_bv['id_bassin']:
    # on récupère l'id du bassin
    id_bassin = id_bv

    # on sélectionne le code du bassin selon son ID et sa superficie
    cursor.execute("""SELECT code_bassin, ST_AREA(geom_2154) FROM bassin
                    WHERE id_bassin = %s;
                    """ % (id_bv))
    rsl = cursor.fetchone()
    code_bassin = rsl[0]
    sup_bassin = rsl[1] / 1000000

    # on vérifie si le bassin est drainé ou non
    cursor.execute("""SELECT type_bassin FROM bassin
                    WHERE id_bassin = %s;
                    """ % (id_bv))
    type_bassin = cursor.fetchone()
    type_bassin = type_bassin[0]

    # si le bassin est drainé, on calcule la densité de drainage du réseau en km
    if type_bassin == 'draine':
        # on sélectionne les tronçons hydro qui appartiennent au bassin
        hydro_xtr = hydro[hydro[champ_code_bv] == code_bassin]
        # on calcule la longueur de chaque tronçon en km
        hydro_xtr['long'] = hydro_xtr['geometry'].length / 1000
        # on calcule la somme des longueurs
        somme_long = hydro_xtr['long'].sum()
        dd = somme_long / sup_bassin

    # sinon la densité de drainage vaut 0
    else:
        dd = 0

    # construction des champs date_inf et date_sup
    date_inf = datetime.strptime(date_hydro, "%d-%m-%Y")
    date_sup = date_inf
    # construction du champ validite
    validite = 'mise_a_jour'

    # on remplit la table pression_potamologique pour la densité de drainage
    cursor.execute("""INSERT INTO pression_potamologique (id_bassin, id_param, valeur, date_inf, date_sup, validite, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s, %s, %s);
                    """, [id_bassin, id_param, dd, date_inf, date_sup, validite, param_id_source])
    conn.commit()


print('This is the End for the Densité de drainage!')