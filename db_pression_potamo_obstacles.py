# -*- coding: utf-8 -*-
# calcul du nombre d'obstacles à l'écoulement par bassin classés en 5 types: barrage, seuil, digue, pont, épis
# la couche des obstacles à l'écoulement doit être en gpkg
import pandas as pd
import geopandas as gpd
import psycopg2
import numpy as np
from datetime import datetime
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# chemin vers la couche des obstacles
path_obst = '/home/passy/SIG/ObstaclesEcoulement/ObstaclesEcoulement.gpkg'

#  le nom du paramètre
param_nom = 'obstacles_ecoulement'

# la date associée à la couche des tronçons hydro (jj-mm-yyyy, en string)
date_obstacles = '01-01-2020'

# les paramètres du paramètre
param_nom = ['nb_barrages', 'nb_seuils', 'nb_digues', 'nb_ponts', 'nb_epis']
param_unite = 'compte'
param_famille = 'obstacles'
param_methode_calc = 'db_pression_potamo_obstacles.py'
param_id_source = 1


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_potamologique' ################
# on vérifie si les paramètres existent déjà ou pas
for param_nom in param_nom:
    cursor.execute("""SELECT id_param FROM param_potamologique
                    WHERE nom_param = %s;
                    """, [param_nom])
    rsl = cursor.fetchall()
    if len(rsl) == 0:
        id_param = fill_tab_param_potamo(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source)


################ calcul du nombre d'obstacles par type et par bassin et remplissage de la table 'pression_potamologique' ################
# chargement de la couche des bassins
sql = "SELECT * FROM bassin"
df_bv = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154' )

# chargement de la couche des obstacles
df_obst = gpd.read_file(path_obst)
# on conserve les colonnes qui vont bien
df_obst = df_obst[['CdObstEcoul','CdEtOuvrage', 'CdTypeOuvrage', 'geometry']]
# on supprime les obstacles avec des 'vides' pour CdEtOuvrage et CdTypeOuvrage
df_obst = df_obst[df_obst['CdEtOuvrage'] != '']
df_obst = df_obst[df_obst['CdTypeOuvrage'] != '']
# on filtre sur les obstacles existants (CdEtOuvrage = 2)
df_obst = df_obst[df_obst['CdEtOuvrage'] == '2']

# on créé une colonne 'type' qui prend les valeurs 'barrage', 'seuil', 'digue', 'pont', 'epis'
# on définit une fonction qui associe les types avec les codes 'CdEtOuvrage'
def create_type(row):
    if row['CdTypeOuvrage'].startswith('1.1'):
        obst_type = 'barrage'
    elif row['CdTypeOuvrage'].startswith('1.2'):
        obst_type = 'seuil'
    elif row['CdTypeOuvrage'].startswith('1.3'):
        obst_type = 'digue'
    elif row['CdTypeOuvrage'].startswith('1.4') and '1.4.2' not in row['CdTypeOuvrage'] and '1.4.3' not in row['CdTypeOuvrage']:
        obst_type = 'pont'
    elif row['CdTypeOuvrage'].startswith('1.5'):
        obst_type = 'epi'
    else:
        obst_type = 'autre'
    return obst_type

# on applique la fonction d'association de type
df_obst['type'] = df_obst.apply(create_type, axis=1)
# on filtre les obstacles non intéressants
df_obst = df_obst[df_obst['type'] != 'autre']
# on associe un index spatial
df_obst.sindex

# on associe à chaque obstacle le bassin dans lequel il se situe via une jointure spatiale
df_obst_bv = gpd.sjoin(df_bv, df_obst, how='right', op='contains')
# on conserve seulement les colonnes utiles
df_obst_bv = df_obst_bv[['CdObstEcoul', 'CdEtOuvrage', 'CdTypeOuvrage', 'type', 'id_bassin']]

# on groupe par bassin et par type en sommant les obstacles
obst_bv_type = df_obst_bv.groupby(['id_bassin', 'type'], as_index=False)['CdObstEcoul'].count()

# insertion des valeurs dans la table 'pression_potamologique'
# on parcourt le df des obstacles regroupés par type et par bassin
for l in range(len(obst_bv_type)):
    # id du bassin
    id_bv = obst_bv_type.iloc[l]['id_bassin']
    # le nombre d'obstacles pour le type rencontré
    val = float(obst_bv_type.iloc[l]['CdObstEcoul'])
    # définition de la validité
    validite = 'mise_a_jour'
    # mise en forme de la date
    date_obst = datetime.strptime(date_obstacles, "%d-%m-%Y")

    # on récupère l'id du paramètre correspondant au type rencontré
    type_obst = '%' + obst_bv_type.iloc[l]['type'] + '%' # transformation du type pour l'interroger via LIKE dans la requête
    cursor.execute("""SELECT id_param FROM param_potamologique
                    WHERE nom_param LIKE %s;
                    """, [type_obst])
    id_param = cursor.fetchone()[0]

    # insertion dans la table pression
    cursor.execute("""INSERT INTO pression_potamologique (id_bassin, id_param, valeur, date_inf, date_sup, validite, id_source)
                VALUES
                  (%s, %s, %s, %s, %s, %s, %s);
                """, [id_bv, id_param, val, date_obst, date_obst, validite, param_id_source])
    conn.commit()



print('This is the End for the Obstacles!')