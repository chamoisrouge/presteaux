# -*- coding: utf-8 -*-
# calcul de la superficie totale des forêts de Cassini pour chaque bassin, remplissage des tables 'param_terrestre' et 'pression_terrestre'
import geopandas as gpd
import psycopg2
from datetime import datetime
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin vers la couche des forêts de Cassini
path_cassini = '/home/passy/SIG/Forets_cassini/forets_cassini.gpkg'

# la date associée à la couche servant à calculer les superficies (jj-mm-yyyy, en string)
date_cassini = '01-01-1870'
validite = 'mise_a_jour'

# les paramètres du paramètre
param_nom = 'forets_cassini'
param_unite = 'km2'
param_famille = 'usage du sol'
param_methode_calc = 'db_pression_terrestre_cassini.py'
param_id_source = 11


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_terrestre' ################
# on vérifie si le paramètre existe déjà ou pas
cursor.execute("""SELECT id_param FROM param_terrestre
                WHERE nom_param = %s;
                """, [param_nom])
rsl = cursor.fetchone()
id_param = rsl
if len(rsl) == 0:
    id_param = fill_tab_param_terrestre(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source)


################ calcul des superficies de forêts de Cassini et remplissage de la table 'pression_terrestre' ################
# on charge la couche des forêts dans un geodf
cassini = gpd.read_file(path_cassini)

# on charge la table des bassins dans un geodf
sql = "SELECT * FROM bassin;"
df_bv = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

# on définit les dates de validité
date_inf = datetime.strptime(date_cassini, "%d-%m-%Y")
date_sup = date_inf

# on parcourt les bv et on intersecte bv par bv en suivant cette méthode : https://geoffboeing.com/2016/10/r-tree-spatial-index-python/
# on liste les id des bassins
cursor.execute("""SELECT id_bassin FROM bassin; """)
lst_id_bv = cursor.fetchall()

# on récupère l'index spatial des forêts de Cassini
spatial_index_cassini = cassini.sindex

# on parcourt les bassins
for id_bv in lst_id_bv:
    # on sélectionne le bv correspondant et on le sauve en geodf
    sql = "SELECT * FROM bassin WHERE id_bassin = %s;" % id_bv
    df_bv = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

    # on récupère les max et min de l'enveloppe du bv en cours et on les met dans un tuple
    bv_bounds = (df_bv.bounds.iloc[0]['minx'], df_bv.bounds.iloc[0]['miny'], df_bv.bounds.iloc[0]['maxx'],
                 df_bv.bounds.iloc[0]['maxy'])
    # on liste les polygones de forêts de Cassini qui intersectent l'enveloppe du bv. Certains sont dans l'enveloppe mais pas dans le bv.
    possible_matches_index = list(spatial_index_cassini.intersection(bv_bounds))
    # on sélectionne les polygones de forêts de Cassini qui intersectent l'enveloppe
    possible_matches = cassini.iloc[possible_matches_index]
    # on filtre ces polygones de forêts pour ne retenir que ceux qui intersectent vraiment le bv
    # precise_matches = possible_matches[possible_matches.intersects(df_bv)]

    # on vérifie si il y a bien des forêts sous jacente au bassin sinon on passe au bassin suivant
    if len(possible_matches) == 0:
        continue

    # on intersecte le bv en question et les forêts de Cassini qui sont dans l'enveloppe
    bv_cassini = gpd.overlay(df_bv, possible_matches, how='intersection')

    # on calcule la superficie de chaque portion de forêt intersectée
    bv_cassini['sup_km2'] = bv_cassini['geometry'].area / 1000000
    # on calcule la somme des superficies de forêts
    sum_sup = bv_cassini['sup_km2'].sum()

    #  on remplit la table pression_terrestre avec la superficie de forêt
    cursor.execute("""INSERT INTO pression_terrestre (id_bassin, id_param, valeur, date_inf, date_sup, validite, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s, %s, %s);
                    """, [id_bv, id_param, sum_sup, date_inf, date_sup, validite, param_id_source])
    conn.commit()

print('This is the End for Cassini!')