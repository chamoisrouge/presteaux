# -*- coding: utf-8 -*-
# calcul de la densité de population pour chaque bassin, remplissage des tables 'param_terrestre' et 'pression_terrestre'
import geopandas as gpd
import psycopg2
from datetime import datetime
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin vers la couche des populations communales
path_pop = '/home/passy/SIG/Insee_pop/pop_municipale_france_insee_L93.gpkg'

# la liste des années à traiter
lst_an = [1954, 1936, 1931, 1926, 1921, 1911, 1906, 1901, 1896, 1891, 1886, 1881, 1876]

# les paramètres du paramètre
param_nom = ['densite_population']
param_unite = 'hab/km2'
param_famille = 'démographie'
param_methode_calc = 'db_pression_terrestre_densite_pop.py'
param_id_source = 12


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_terrestre' ################
# on vérifie si les paramètres existent déjà ou pas
for param in param_nom:
    cursor.execute("""SELECT id_param FROM param_terrestre
                    WHERE nom_param = %s;
                    """, [param])
    rsl = cursor.fetchall()
    if len(rsl) == 0:
        id_param = fill_tab_param_terrestre(conn, cursor, param, param_unite, param_famille, param_methode_calc, param_id_source)
    else:
        id_param = rsl[0]


################ calcul des densités de population et remplissage de la table 'pression_terrestre' ################
# on charge la couche des populations communales en geodf
pop = gpd.read_file(path_pop)

# on charge la table des bassins en geodf
sql = "SELECT * FROM bassin;"
df_bv = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

# on parcourt la liste des années à traiter
for an in lst_an:
    # on construit les dates de début et de fin et la validité
    dinf = '01-01-' + str(an)
    dsup = '31-12-' + str(an)
    date_inf = datetime.strptime(dinf, "%d-%m-%Y")
    date_sup = datetime.strptime(dsup, "%d-%m-%Y")
    validite = 'periode'

    # on cherche la colonne de l'année souhaitée
    if an > 2000:
        nom_col = 'PMUN' + str(an)[2:]
    elif an > 1954:
        nom_col = 'PSDC' + str(an)[2:]
    elif an > 1931:
        nom_col = 'PTOT' + str(an)[2:]
    else:
        nom_col = 'PTOT' + str(an)
    # on garde les colonnes qui nous intéressent
    pop_xtr = pop.loc[:, ['INSEE_COM', nom_col, 'geometry']]

    # on calcule la densité de population pour chaque commune
    pop_xtr['densite'] = pop_xtr[nom_col] / (pop_xtr['geometry'].area / 1000000)

    # on parcourt les bv et on intersecte bv par bv en suivant cette méthode : https://geoffboeing.com/2016/10/r-tree-spatial-index-python/
    # on liste les id des bassins
    cursor.execute("""SELECT id_bassin FROM bassin; """)
    lst_id_bv = cursor.fetchall()
    # print(lst_id_bv)

    # on récupère l'index spatial des communes
    spatial_index_communes = pop_xtr.sindex

    # on parcourt les bassins
    for id_bv in lst_id_bv:
        # on vérifie si le bassin n'a pas déjà été fait dans un run précédent, si c'est le cas on passe au bassin suivant
        cursor.execute("""SELECT valeur FROM pression_terrestre
                        WHERE id_bassin = %s AND id_param = %s AND date_inf = %s AND date_sup = %s;
                        """, [id_bv, id_param, date_inf, date_sup])
        rsl = cursor.fetchall()
        if len(rsl) > 0:
            continue

        # on sélectionne le bv correspondant et on le sauve en df
        sql = "SELECT * FROM bassin WHERE id_bassin = %s;" % id_bv
        df_bv = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

        # on récupère les max et min de l'enveloppe du bv en cours et on les met dans un tuple
        bv_bounds = (df_bv.bounds.iloc[0]['minx'], df_bv.bounds.iloc[0]['miny'], df_bv.bounds.iloc[0]['maxx'],
                     df_bv.bounds.iloc[0]['maxy'])
        # on liste les communes qui intersectent l'enveloppe du bv. Certaines sont dans l'enveloppe mais pas dans le bv.
        possible_matches_index = list(spatial_index_communes.intersection(bv_bounds))
        # on sélectionne les communes qui intersectent l'enveloppe
        possible_matches = pop_xtr.iloc[possible_matches_index]

        #  on vérifie si il y a bien des communes sous jacentes au bassin sinon on passe au bassin suivant
        if len(possible_matches) == 0:
            continue

        # on intersecte le bv en question et les communes qui sont dans l'enveloppe
        bv_communes = gpd.overlay(df_bv, possible_matches, how='intersection')

        # on calcule l'aire de chaque polygone intersecté
        bv_communes['sup_km2'] = bv_communes['geometry'].area / 1000000
        # on calcule le nombre d'habitants au sein de chaque polygone
        bv_communes['hab'] = bv_communes['densite'] * bv_communes['sup_km2']
        # on regroupe par id_bassin en faisant une somme sur le nombre d'habitants
        bv_communes_pop = bv_communes.groupby(['id_bassin'], as_index=False)['sup_km2', 'hab'].sum()
        # on calcule la densité de population pour le bassin
        dens_pop_bv = bv_communes_pop['hab'] / bv_communes_pop['sup_km2']

        #  on remplit la table pression_terrestre pour le paramètre correspondant à la densité
        cursor.execute("""INSERT INTO pression_terrestre (id_bassin, id_param, valeur, date_inf, date_sup, validite, id_source)
                        VALUES
                          (%s, %s, %s, %s, %s, %s, %s);
                        """, [id_bv, id_param, dens_pop_bv[0], date_inf, date_sup, validite, param_id_source])
        conn.commit()

        #break


print('This is the End for the Densités!')