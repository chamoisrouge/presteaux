# -*- coding: utf-8 -*-
# calcul de la pente médiane et du coeeficient interquartile relatif des pentes pour chaque bassin, remplissage des tables 'param_terrestre' et 'pression_terrestre'
import pandas as pd
import geopandas as gpd
import psycopg2
import numpy as np
from datetime import datetime
import rasterio
from rasterstats import zonal_stats
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin verst le raster des pentes
path_pentes = '/home/passy/SIG/SRTM_30m/mos_srtm_pentes_L93.tif'

# la date associée à la couche servant à calculer les superficies (jj-mm-yyyy, en string)
date_alti = '01-03-2000'

# les paramètres du paramètre
param_nom = ['pente_mediane', 'pente_coef_IQ_relatif']
param_unite = 'degrés'
param_famille = 'topographie'
param_methode_calc = 'db_pression_terrestre_penes.py'
param_id_source = 4


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_terrestre' ################
# on vérifie si les paramètres existent déjà ou pas
for param in param_nom:
    cursor.execute("""SELECT id_param FROM param_terrestre
                    WHERE nom_param = %s;
                    """, [param])
    rsl = cursor.fetchall()
    if len(rsl) == 0:
        id_param = fill_tab_param_terrestre(conn, cursor, param, param_unite, param_famille, param_methode_calc, param_id_source)


################ calcul des stats des pentes et remplissage de la table 'pression_terrestre' ################
# on charge le raster des pentes
pentes = rasterio.open(path_pentes)
# on récupère ses paramètres de projection
transform = pentes.transform
# on récupère ses valeurs dans un array
array_pentes = pentes.read(1)
# on filtre les valeurs manquantes codées à -32768 à -999 qui est la valeur standard des NaN dans le module rasterstats
array_pentes = np.where(array_pentes == -32768, -999, array_pentes)

# on liste les id des bassins
cursor.execute("""SELECT id_bassin FROM bassin; """)
lst_id_bv = cursor.fetchall()

# on définit la date et la validite
date_inf = datetime.strptime(date_alti, "%d-%m-%Y")
date_sup = date_inf
validite = 'mise_a_jour'

# on parcourt les bv
for id_bv in lst_id_bv:
    # on sélectionne le bv correspondant et on le sauve en geodf
    sql = "SELECT * FROM bassin WHERE id_bassin = %s;" %id_bv
    df_bv = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154' )
    # on calcule les stats zonales pour le bv en cours
    zs = zonal_stats(df_bv, array_pentes, affine=transform, stats=['median', 'percentile_25', 'percentile_75'])
    # on récupère la médiane
    pente_mediane = zs[0]['median']
    # on calcule le coefficient interquartile relatif
    pente_iq_rel = (zs[0]['percentile_75'] - zs[0]['percentile_25']) / zs[0]['median']

    # on insère ces valeurs dans la table 'pression_terrestre' pour les paramètres correspondant
    # on parcourt les param
    for param in param_nom:
        #  on récupère l'id du param en cours
        cursor.execute("""SELECT id_param FROM param_terrestre
                        WHERE nom_param = %s;
                        """, [param])
        id_param = cursor.fetchone()
        # on aiguille selon le paramètre
        if param == 'pente_mediane':
            val = pente_mediane
        elif param == 'pente_coef_IQ_relatif':
            val = pente_iq_rel

        #  on remplit la table pression_terrestre pour le paramètre de pente correspondant
        cursor.execute("""INSERT INTO pression_terrestre (id_bassin, id_param, valeur, date_inf, date_sup, validite, id_source)
                        VALUES
                          (%s, %s, %s, %s, %s, %s, %s);
                        """, [id_bv, id_param, val, date_inf, date_sup, validite, param_id_source])
        conn.commit()


print('This is the End for the Pentes!')