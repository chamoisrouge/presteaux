# -*- coding: utf-8 -*-
# calcul de la somme des pressions liées aux STEU pour chaque bassin, remplissage des tables 'param_terrestre' et 'pression_terrestre'
import pandas as pd
import geopandas as gpd
import psycopg2
from datetime import datetime
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin vers le répertoire contenant les csv des STEU. Le nom est de type 'Export_ERU_yyyy.csv'
path_steu = '/home/passy/SIG/step/'

# la liste des années à traiter
lst_an = [2009]

# les paramètres du paramètre
param_nom = ['max_somme_pollutions_entrantes', 'somme_capacite_nominale', 'capacite_nominale', 'capacite_nominale_dbo5', 'debit_reference', \
             'charge_max_entrante', 'debit_entrant', 'somme_pollutions_entrantes', 'pollution_entrante']
param_unite = ['equivalent habitants', 'equivalent habitants', 'equivalent habitants', 'kg DBO5', 'm3/jour', 'equivalent habitants', 'm3/jour', 'equivalent habitants', 'equivalent habitants']
param_famille = 'steu'
param_methode_calc = 'db_pression_terrestre_steu.py'
param_id_source = 45


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_terrestre' ################
# on vérifie si les paramètres existent déjà ou pas
p = 0
for param in param_nom:
    cursor.execute("""SELECT id_param FROM param_terrestre
                    WHERE nom_param = %s;
                    """, [param])
    rsl = cursor.fetchall()
    if len(rsl) == 0:
        unite = param_unite[p]
        id_param = fill_tab_param_terrestre(conn, cursor, param, unite, param_famille, param_methode_calc, param_id_source)
    else:
        id_param = rsl[0]
    p+=1


################ calcul des pressions liées aux STEU et remplissage de la table 'pression_terrestre' ################
# on récupère la couche des bassins dans un geodf
sql = "SELECT * FROM bassin;"
df_bv = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

# on parcourt les années à traiter
for an in lst_an:
    # on traite le cas particulier de l'année 2009 (fichier d'entrée différent)
    if an == 2009:
        #  on construit les dates de début et de fin et la validité
        dinf = '01-01-' + str(an)
        dsup = '31-12-' + str(an)
        date_inf = datetime.strptime(dinf, "%d-%m-%Y")
        date_sup = datetime.strptime(dsup, "%d-%m-%Y")
        validite = 'periode'

        # on importe le csv de l'année 2009 et seulement les colonnes utiles
        df_steu = pd.read_csv(path_steu + 'Export_ERU_' + str(an) + '.csv', sep=';',
                              usecols=['Somme des pollutions entrantes (EH)', \
                                       'Somme des capacités nominales (EH)', 'Pollution entrante (EH)',  'Capacité nominale en EH', \
                                       'Débit de référence en m3/j', 'Débit entrant en m3/j', \
                                       'Longitude du rejet (WGS84)', 'Latitude du rejet (WGS84)'])

        # on transforme ce df en geodf
        df_steu = gpd.GeoDataFrame(df_steu, geometry=gpd.points_from_xy(df_steu['Longitude du rejet (WGS84)'],
                                                                        df_steu['Latitude du rejet (WGS84)']))
        # on spécifie que les coordonnées sont en WGS84 4326
        df_steu.crs = {'init': 'epsg:4326'}

        # on reprojecte ce geodf en 2154
        df_steu = df_steu.to_crs(epsg=2154)

        # on joint spatialement les STEU et les BV
        df_steu_bv = gpd.sjoin(df_steu, df_bv, how='inner', op='within')

        # on somme les critères quantitatifs par bassin
        df_steu_bv_pression = df_steu_bv.groupby('id_bassin')[['Somme des pollutions entrantes (EH)', \
                                       'Somme des capacités nominales (EH)', 'Pollution entrante (EH)',  'Capacité nominale en EH', \
                                       'Débit de référence en m3/j', 'Débit entrant en m3/j']].sum()
        df_steu_bv_pression = df_steu_bv_pression.reset_index()

        # insertion des valeurs de pressions liées aux STEU dans la table pression_terrestre
        # on parcourt les bassins
        for id_bassin in df_steu_bv_pression['id_bassin']:
            #  on parcourt les params
            for param in param_nom:
                #  on récupère l'id du param en cours
                cursor.execute("""SELECT id_param FROM param_terrestre
                                    WHERE nom_param = %s;
                                    """, [param])
                id_param = cursor.fetchone()
                # on aiguille selon le paramètre
                if param == 'somme_pollutions_entrantes':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Somme des pollutions entrantes (EH)'])
                elif param == 'pollution_entrante':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Pollution entrante (EH)'])
                elif param == 'somme_capacite_nominale':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Somme des capacités nominales (EH)'])
                elif param == 'capacite_nominale':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Capacité nominale en EH'])
                elif param == 'debit_reference':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Débit de référence en m3/j'])
                elif param == 'debit_entrant':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Débit entrant en m3/j'])
                # si il n'y pas de valeur associée au paramètre en cours, on passe au para suivant
                else:
                    continue

                #  on remplit la table pression_terrestre pour le paramètre de STEU correspondant
                cursor.execute("""INSERT INTO pression_terrestre (id_bassin, id_param, valeur, date_inf, date_sup, validite, id_source)
                                    VALUES
                                      (%s, %s, %s, %s, %s, %s, %s);
                                    """, [id_bassin, id_param, val, date_inf, date_sup, validite, param_id_source])
                conn.commit()
                print(an)

    # pour toutes les autres années
    else:
        # on construit les dates de début et de fin et la validité
        dinf = '01-01-' + str(an)
        dsup = '31-12-' + str(an)
        date_inf = datetime.strptime(dinf, "%d-%m-%Y")
        date_sup = datetime.strptime(dsup, "%d-%m-%Y")
        validite = 'periode'

        # on importe le csv de l'année en cours et seulement les colonnes utiles
        df_steu = pd.read_csv(path_steu + 'Export_ERU_' + str(an) + '.csv', sep=';', usecols=['Maximum de la somme des pollutions entrantes (EH)', \
                                                           'Somme des capacités nominales (EH)', 'Capacité nominale en EH', \
                                                           'Capacité nominale en Kg de DBO5', 'Débit de référence en m3/j', \
                                                           'Charge maximale entrante (EH)', 'Débit entrant en m3/j', \
                                                           'Coordonnée X du rejet', 'Coordonnée Y du rejet'])

        # on transforme ce df en geodf
        df_steu = gpd.GeoDataFrame(df_steu, geometry=gpd.points_from_xy(df_steu['Coordonnée X du rejet'],
                                                                        df_steu['Coordonnée Y du rejet']), crs='epsg:2154')

        # on joint spatialement les STEU et les BV
        df_steu_bv = gpd.sjoin(df_steu, df_bv, how='inner', op='within')

        # on somme les critères quantitatifs par bassin
        df_steu_bv_pression = df_steu_bv.groupby('id_bassin')[['Maximum de la somme des pollutions entrantes (EH)',\
                                                               'Somme des capacités nominales (EH)', \
                                                               'Capacité nominale en EH', \
                                                               'Capacité nominale en Kg de DBO5', \
                                                               'Débit de référence en m3/j', \
                                                               'Charge maximale entrante (EH)', \
                                                               'Débit entrant en m3/j']].sum()
        df_steu_bv_pression = df_steu_bv_pression.reset_index()

        # insertion des valeurs de pressions liées aux STEU dans la table pression_terrestre
        # on parcourt les bassins
        for id_bassin in df_steu_bv_pression['id_bassin']:
            #  on parcourt les params
            for param in param_nom:
                #  on récupère l'id du param en cours
                cursor.execute("""SELECT id_param FROM param_terrestre
                                    WHERE nom_param = %s;
                                    """, [param])
                id_param = cursor.fetchone()
                # on aiguille selon le paramètre
                if param == 'max_somme_pollutions_entrantes':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Maximum de la somme des pollutions entrantes (EH)'])
                elif param == 'somme_capacite_nominale':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Somme des capacités nominales (EH)'])
                elif param == 'capacite_nominale':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Capacité nominale en EH'])
                elif param == 'capacite_nominale_dbo5':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Capacité nominale en Kg de DBO5'])
                elif param == 'debit_reference':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Débit de référence en m3/j'])
                elif param == 'charge_max_entrante':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Charge maximale entrante (EH)'])
                elif param == 'debit_entrant':
                    val = float(df_steu_bv_pression.query('id_bassin == %s' % (id_bassin))['Débit entrant en m3/j'])

                #  on remplit la table pression_terrestre pour le paramètre de STEU correspondant
                cursor.execute("""INSERT INTO pression_terrestre (id_bassin, id_param, valeur, date_inf, date_sup, validite, id_source)
                                    VALUES
                                      (%s, %s, %s, %s, %s, %s, %s);
                                    """, [id_bassin, id_param, val, date_inf, date_sup, validite, param_id_source])
                conn.commit()

print('This is the End for the STEU!')