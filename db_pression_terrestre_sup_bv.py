# -*- coding: utf-8 -*-
# calcul de la superficie de chaque bassin, remplissage des tables 'param_terrestre' et 'pression_terrestre'
import pandas as pd
import geopandas as gpd
import psycopg2
import numpy as np
from datetime import datetime
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# la date associée à la couche servant à calculer les superficies (jj-mm-yyyy, en string)
date_bv = '01-01-2016'

# les paramètres du paramètre
param_nom = 'superficie'
param_unite = 'km2'
param_famille = 'hydrographie'
param_methode_calc = 'db_pression_terrestre_sup.py'
param_id_source = 3


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_terrestre' ################
# on vérifie si le paramètre existe déjà ou pas
cursor.execute("""SELECT id_param FROM param_terrestre
                WHERE nom_param = %s;
                """, [param_nom])
rsl = cursor.fetchall()
if len(rsl) == 0:
    id_param = fill_tab_param_terrestre(conn, cursor, param_nom, param_unite, param_famille, param_methode_calc, param_id_source)
else:
    id_param = rsl[0]

################ calcul de la superfice et remplissage de la table 'pression_terrestre' ################
# on récupère la table des bassins sous forme d'un dataframe
df_bv = pd.read_sql_query('SELECT id_bassin FROM "bassin"', con=conn)

# on parcourt la liste des id bv
for id_bv in df_bv['id_bassin']:
    # on récupère l'id du bassin
    id_bassin = id_bv

    # on sélectionne le code du bassin selon son ID et sa superficie
    cursor.execute("""SELECT ST_AREA(geom_2154) FROM bassin
                    WHERE id_bassin = %s;
                    """ % (id_bv))
    rsl = cursor.fetchone()
    sup_bassin = rsl[0] / 1000000

    # construction des champs date_inf et date_sup
    date_inf = datetime.strptime(date_bv, "%d-%m-%Y")
    date_sup = date_inf
    # construction du champ validite
    validite = 'mise_a_jour'

    # on remplit la table pression_terrestre pour la superficie du bassin
    cursor.execute("""INSERT INTO pression_terrestre (id_bassin, id_param, valeur, date_inf, date_sup, validite, id_source)
                    VALUES
                      (%s, %s, %s, %s, %s, %s, %s);
                    """, [id_bassin, id_param, sup_bassin, date_inf, date_sup, validite, param_id_source])
    conn.commit()


print('This is the End for the Superficie!')