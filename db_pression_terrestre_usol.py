# -*- coding: utf-8 -*-
# calcul de la superficie totale de chaque classe d'uasge du sol pour chaque bassin, remplissage des tables 'param_terrestre' et 'pression_terrestre'
# nécessite un fichier csv annexe en deux colonnes spérarées par un ';' tel que : (remplacer 'classe_usol_n' par un nom personnalisé
# pixel;nom_param
# 1;classe_usol_1
# n:classe_usol_n
import pandas as pd
import geopandas as gpd
import psycopg2
import numpy as np
from datetime import datetime
import rasterio
from rasterio.mask import mask
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin vers le raster d'usage du sol
path_usol = '/home/passy/SIG/OSO_Theia/OCS_2019_CESBIO.tif'

# les dates de début et de fin associées à la couche (jj-mm-yyyy, en string)
date_usol_start = '01-01-2019'
date_usol_end = '31-12-2019'
validite = 'periode'

# les paramètres du paramètre
# le chemin vers le csv contenant les valeurs de pixels et les noms des params
path_param = '/home/passy/SIG/OSO_Theia/param_theia_2018-2019.csv'
# le nom du paramètre qui compte le nombre de pixels pour cet usage du sol par bassin
param_nb_pix = 'theia_nb_pix'
# le préfixe du paramètre de cet usage du sol
pref_param = 'ocs_theia_'
param_unite = 'km2'
param_famille = 'usage du sol'
param_methode_calc = 'db_pression_terrestre_usol.py'
param_id_source = 42


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_terrestre' ################
# on charge le csv contenant le nom des paramètres
param_df = pd.read_csv(path_param, sep=';')

# on vérifie si les paramètres existent déjà ou pas
for param in param_df['nom_param']:
    cursor.execute("""SELECT id_param FROM param_terrestre
                    WHERE nom_param = %s and id_source = %s;
                    """, [param, param_id_source])
    rsl = cursor.fetchall()
    if len(rsl) == 0:
        id_param = fill_tab_param_terrestre(conn, cursor, param, param_unite, param_famille, param_methode_calc, param_id_source)

# on ajoute un paramètre de compte de pixels pour l'usage du sol considéré
# on vérifie si ce paramètre existe déjà ou pas
cursor.execute("""SELECT id_param FROM param_terrestre
                WHERE nom_param = %s and id_source = %s;
                """, [param_nb_pix, param_id_source])
rsl = cursor.fetchall()
if len(rsl) == 0:
    id_param = fill_tab_param_terrestre(conn, cursor, param_nb_pix, 'nombre de pixels', param_famille, param_methode_calc,
                                        param_id_source)


################ calcul des superficies totales de chaque classe d'usage du sol et remplissage de la table 'pression_terrestre' ################
# on charge le raster d'usage du sol
usol = rasterio.open(path_usol)
# on récupère la résolution
res = usol.res

# on liste les id des bassins
cursor.execute("""SELECT id_bassin FROM bassin; """)
lst_id_bv = cursor.fetchall()

# on définit les dates de début et de fin et la validite
date_inf = datetime.strptime(date_usol_start, "%d-%m-%Y")
date_sup = datetime.strptime(date_usol_end, "%d-%m-%Y")

# on parcourt les bassins
for id_bv in lst_id_bv:
    # on sélectionne le bassin correspondant et on le sauve en df
    sql = "SELECT * FROM bassin WHERE id_bassin = %s;" %id_bv
    df_bv = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154')

    # on masque l'usage du sol avec le bassin en cours
    usol_bv, out_transform = mask(dataset=usol, shapes=getFeatures(df_bv), crop=True)

    # on compte les occurrences des différentes classes d'usages du sol dans le bassin
    unique, counts = np.unique(usol_bv, return_counts=True)
    d = dict(zip(unique, counts))

    # on fait la somme du compte de tous les pixels disponibles afin de voir si le bassin est bien couvert par l'usage du sol
    cpt = 0
    for val_pix in (d.keys()):
        # on ne s'intéresse qu'aux valeurs de pixels qui nous intéressent
        if val_pix in list(param_df['pixel']):
            cpt = cpt + d[val_pix]

    # on insère ce compte de pixels dans la table pour le paramètre qui va bien
    cursor.execute("""SELECT id_param FROM param_terrestre 
            WHERE nom_param = %s AND id_source = %s;
            """, [param_nb_pix, param_id_source])
    id_param = cursor.fetchone()
    cursor.execute("""INSERT INTO pression_terrestre (id_bassin, id_param, valeur, date_inf, date_sup, validite, id_source)
            VALUES
            (%s, %s, %s, %s, %s, %s, %s);
            """, [id_bv, id_param, int(cpt), date_inf, date_sup, validite, param_id_source])
    conn.commit()

    # si il y a plus de 0 pixel d'usage du sol dans le bassin, on compte le nombre de pixels de chaque classe d'usage du sol
    if cpt > 0:
        # on insère les superficies d'usage du sol du bassin dans la table 'pression_terrestre'
        for val_pix in (d.keys()):
            # on ne s'intéresse qu'aux valeurs de pixels qui nous intéressent
            if val_pix in list(param_df['pixel']):
                # on récupère l'id du param correspondant à la valeur du pixel
                cursor.execute("""SELECT id_param FROM param_terrestre 
                WHERE nom_param = %s AND id_source = %s;
                """, [pref_param + str(val_pix), param_id_source])
                id_param = cursor.fetchone()

                # on calcule la superficie de l'usage du sol en cours
                val = d[val_pix] * ((res[0] / 1000) * (res[1] / 1000))

                cursor.execute("""INSERT INTO pression_terrestre (id_bassin, id_param, valeur, date_inf, date_sup, validite, id_source)
                    VALUES
                    (%s, %s, %s, %s, %s, %s, %s);
                    """, [id_bv, id_param, val, date_inf, date_sup, validite, param_id_source])
                conn.commit()

print('This is the End for the Usol!')