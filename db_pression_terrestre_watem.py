# -*- coding: utf-8 -*-
# calcul de la valeur médiane de l'indice WATEM/SEDEM et de son coeeficient interquartile relatif pour chaque bassin, remplissage des tables 'param_terrestre' et 'pression_terrestre'
import pandas as pd
import geopandas as gpd
import psycopg2
import numpy as np
from datetime import datetime
import rasterio
from rasterstats import zonal_stats
from db_generic_functions import *

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le chemin vers le raster WATEM/SEDEM
path_watem = '/home/passy/SIG/Watem_Sedem/WATEM_Code_Mean_t_ha_yr_100m_france_L93.tif'

# la date associée à la couche servant à calculer les superficies (jj-mm-yyyy, en string)
date_watem = '01-01-2010'

# les paramètres du paramètre
param_nom = ['watem_mediane', 'watem_coef_IQ_relatif', 'watem_nb_pix']
param_unite = ['Mg/ha/an', 'Mg/ha/an', 'nombre de pixels']
param_famille = 'érosion'
param_methode_calc = 'db_pression_terrestre_watem.py'
param_id_source = 4


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table 'param_terrestre' ################
# on vérifie si les paramètres existent déjà ou pas
for param, unite in zip(param_nom, param_unite):
    cursor.execute("""SELECT id_param FROM param_terrestre
                    WHERE nom_param = %s;
                    """, [param])
    rsl = cursor.fetchall()
    if len(rsl) == 0:
        id_param = fill_tab_param_terrestre(conn, cursor, param, unite, param_famille, param_methode_calc, param_id_source)


################ calcul des stats de l'indice WaREM/SEDEM et remplissage de la table 'pression_terrestre' ################
# on charge le raster
watem = rasterio.open(path_watem)
# on récupère ses paramètres de projection
transform = watem.transform
# on récupère ses valeurs dans un array
array_watem = watem.read(1)
# on filtre les valeurs manquantes codées à --3.4028234663852885981e+38 à -999 qui est la valeur standard des NaN dans le module rasterstats
array_watem = np.where(array_watem < -10000, -999, array_watem)

# on liste les id des bassins
cursor.execute("""SELECT id_bassin FROM bassin; """)
lst_id_bv = cursor.fetchall()

# on définit la date et la validite
date_inf = datetime.strptime(date_watem, "%d-%m-%Y")
date_sup = date_inf
validite = 'mise_a_jour'

# on parcourt les bv
for id_bv in lst_id_bv:
    # on sélectionne le bv correspondant et on le sauve en geodf
    sql = "SELECT * FROM bassin WHERE id_bassin = %s;" %id_bv
    df_bv = gpd.GeoDataFrame.from_postgis(sql, conn, geom_col='geom_2154' )
    # on calcule les stats zonales pour le bv en cours
    zs = zonal_stats(df_bv, array_watem, affine=transform, stats=['median', 'percentile_25', 'percentile_75', 'count'])

    # si le bassin n'est pas couvert par le raster WaTEM on remplit juste le nombre de pixels (0) et on passe au suivant
    if zs[0]['percentile_75'] is None:
        #  on insère le nombre de pixels WaTEM pour le bassin en cours
        #  on récupère l'id du param correspondant au nombre de pixels WaTEM
        cursor.execute("""SELECT id_param FROM param_terrestre
                        WHERE nom_param = %s;
                        """, ['watem_nb_pix'])
        id_param_count = cursor.fetchone()
        #  on insère le nombre de pixels
        cursor.execute("""INSERT INTO pression_terrestre (id_bassin, id_param, valeur, date_inf, date_sup, validite, id_source)
                        VALUES
                          (%s, %s, %s, %s, %s, %s, %s);
                        """, [id_bv, id_param_count, zs[0]['count'], date_inf, date_sup, validite, param_id_source])
        conn.commit()
        continue

    # on récupère la médiane
    watem_mediane = zs[0]['median']
    # on calcule le coefficient interquartile relatif
    watem_iq_rel = (zs[0]['percentile_75'] - zs[0]['percentile_25']) / zs[0]['median']

    # on insère ces valeurs dans la table 'pression_terrestre' pour les paramètres correspondant
    # on parcourt les param
    for param in param_nom:
        #  on récupère l'id du param en cours
        cursor.execute("""SELECT id_param FROM param_terrestre
                        WHERE nom_param = %s;
                        """, [param])
        id_param = cursor.fetchone()
        # on aiguille selon le paramètre
        if param == 'watem_mediane':
            val = watem_mediane
        elif param == 'watem_coef_IQ_relatif':
            val = watem_iq_rel
        elif param == 'watem_nb_pix':
            val = zs[0]['count']

        #  on remplit la table pression_terrestre pour le paramètre d'indice WaTEM/SEDEM correspondant
        cursor.execute("""INSERT INTO pression_terrestre (id_bassin, id_param, valeur, date_inf, date_sup, validite, id_source)
                        VALUES
                          (%s, %s, %s, %s, %s, %s, %s);
                        """, [id_bv, id_param, val, date_inf, date_sup, validite, param_id_source])
        conn.commit()



print('This is the End for the WaTEM!')