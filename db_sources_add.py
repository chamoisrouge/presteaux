# -*- coding: utf-8 -*-
# insertion de nouvelles sources dans la table 'sources'
import psycopg2
from db_generic_functions import import_param_conn

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# le nom de la source
name_source = 'test'
# un lien vers un descriptif de la source
link_source = 'https://test.com'
# contact
contact_source = 'Roger'
# commentaire
commentaire_source = 'Super source'


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table sources ################
# insertion des champs
cursor.execute("""INSERT INTO sources (id_source, nom_source, lien, contact, commentaire)
                VALUES
                  (DEFAULT, %s, %s, %s, %s);
                """, [name_source, link_source, contact_source, commentaire_source])
conn.commit()

print('This is the End !')