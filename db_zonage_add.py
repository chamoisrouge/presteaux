# -*- coding: utf-8 -*-
# insertion d'un nouveau zonage dans ta table 'zonage'
import psycopg2
from db_generic_functions import import_param_conn

# le chemin vers le csv des paramètres de connexion à la base
path_conn = '/home/passy/PycharmProjects/presteaux/conn_presteaux.txt'

# la version du zonage
version_zonage = 'rapportage 2018'
# la date du zonage au format 'dd-mm-yyyy'
date_zonage = '01-01-2018'


# on récupère les paramètres de connexion
conn_param = import_param_conn(path_conn)
#  on récupère les paramètres
database = conn_param['database'][0]
user = conn_param['user'][0]
password = conn_param['password'][0]
port = conn_param['port'][0]
# on se connecte à la base
conn = psycopg2.connect(database=database, user=user, password=password, port=port)
cursor = conn.cursor()

################ remplissage de la table zonage ################
# insertion des champs
cursor.execute("""INSERT INTO zonage (version, date)
                VALUES
                  (%s, %s);
                """, [version_zonage, date_zonage])
conn.commit()

print('This is the End!')