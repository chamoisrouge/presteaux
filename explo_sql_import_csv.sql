-- remplacer les chemins
COPY zonage(id_zonage,version,date) 
FROM '/home/passy/DB_presteaux/developpement_tables/tab_zonage.csv' DELIMITER ';' CSV HEADER;

COPY asso_terre(id_mect,id_bassin,id_zonage,connexion) 
FROM '/home/passy/DB_presteaux/developpement_tables/tab_asso_terre.csv' DELIMITER ';' CSV HEADER;

COPY asso_mect(id_mect,id_mect1,id_zonage,connexion) 
FROM '/home/passy/DB_presteaux/developpement_tables/tab_asso_mect.csv' DELIMITER ';' CSV HEADER;

COPY sources(id_source,nom_source,lien,contact,commentaire) 
FROM '/home/passy/DB_presteaux/developpement_tables/tab_sources.csv' DELIMITER ';' CSV HEADER;

COPY param_potamologique(id_param,nom_param,unite,famille,methode_calc,id_source) 
FROM '/home/passy/DB_presteaux/developpement_tables/tab_param_potamologique.csv' DELIMITER ';' CSV HEADER;

COPY param_terrestre(id_param,nom_param,unite,famille,methode_calc,id_source) 
FROM '/home/passy/DB_presteaux/developpement_tables/tab_param_terrestre.csv' DELIMITER ';' CSV HEADER;

COPY param_littoral(id_param,nom_param,unite,famille,methode_calc,id_source) 
FROM '/home/passy/DB_presteaux/developpement_tables/tab_param_littoral.csv' DELIMITER ';' CSV HEADER;

COPY pression_potamologique(id_bassin,id_param,valeur,date_inf,date_sup,validite,id_source) 
FROM '/home/passy/DB_presteaux/developpement_tables/tab_pression_potamologique.csv' DELIMITER ';' CSV HEADER;

COPY pression_terrestre(id_bassin,id_param,valeur,date_inf,date_sup,validite,id_source) 
FROM '/home/passy/DB_presteaux/developpement_tables/tab_pression_terrestre.csv' DELIMITER ';' CSV HEADER;

COPY pression_littoral(id_mect,id_param,valeur,date_inf,date_sup,validite,id_source) 
FROM '/home/passy/DB_presteaux/developpement_tables/tab_pression_littoral.csv' DELIMITER ';' CSV HEADER;

COPY bonus(id_objet,id_mect,code_objet,type_objet,commentaire,geom_point_L93,geom_ligne_L93, geom_polygone_L93, id_bonus_source, version) 
FROM '/home/passy/DB_presteaux/developpement_tables/tab_bonus.csv' DELIMITER ';' CSV HEADER;

COPY bonus_source(id_bonus_source,nom_source,version,lien,commentaire) 
FROM '/home/passy/DB_presteaux/developpement_tables/tab_bonus_source.csv' DELIMITER ';' CSV HEADER;

