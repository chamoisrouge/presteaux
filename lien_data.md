Les données sont stockées sur un serveur accessible à cette adresse :
https://www.metis.upmc.fr/~passy/presteaux/data_presteaux.zip

Dans cette archive, se trouvent :
* <font color=green>un fichier dump</font> : le dump de la base contenant la structure et les données.
* <font color=green>des fichiers csv</font> : les différentes tables au format csv. Il est possible de les importer dans la base vide via le script *explo_sql_import_csv.sql*.
* <font color=green>des fichiers gpkg</font> : les geopackages des bassins et des mects.
