-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.2_snapshot20191127
-- PostgreSQL version: 12.0
-- Project Site: pgmodeler.io
-- Model Author: ---


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: presteaux | type: DATABASE --
-- -- DROP DATABASE IF EXISTS presteaux;
-- CREATE DATABASE presteaux;
-- -- ddl-end --
-- 

-- object: postgis | type: EXTENSION --
-- DROP EXTENSION IF EXISTS postgis CASCADE;
CREATE EXTENSION postgis
;
-- ddl-end --

-- object: public.mect | type: TABLE --
-- DROP TABLE IF EXISTS public.mect CASCADE;
CREATE TABLE public.mect (
	id_mect serial NOT NULL,
	code_mect character varying NOT NULL,
	type_mect character varying NOT NULL,
	geom_2154 geometry(MULTIPOLYGON, 2154) NOT NULL,
	CONSTRAINT mect_pk PRIMARY KEY (id_mect)

);
-- ddl-end --
-- ALTER TABLE public.mect OWNER TO postgres;
-- ddl-end --

-- object: public.bassin | type: TABLE --
-- DROP TABLE IF EXISTS public.bassin CASCADE;
CREATE TABLE public.bassin (
	id_bassin serial NOT NULL,
	code_bassin character varying NOT NULL,
	type_bassin character varying NOT NULL,
	geom_2154 geometry(MULTIPOLYGON, 2154) NOT NULL,
	CONSTRAINT bassin_pk PRIMARY KEY (id_bassin)

);
-- ddl-end --
-- ALTER TABLE public.bassin OWNER TO postgres;
-- ddl-end --

-- object: public.asso_terre | type: TABLE --
-- DROP TABLE IF EXISTS public.asso_terre CASCADE;
CREATE TABLE public.asso_terre (
	id_mect integer NOT NULL,
	id_bassin integer NOT NULL,
	id_zonage integer NOT NULL,
	connexion varchar NOT NULL,
	CONSTRAINT asso_terre_pk PRIMARY KEY (id_mect,id_bassin,id_zonage)

);
-- ddl-end --
-- ALTER TABLE public.asso_terre OWNER TO postgres;
-- ddl-end --

-- object: mect_fk | type: CONSTRAINT --
-- ALTER TABLE public.asso_terre DROP CONSTRAINT IF EXISTS mect_fk CASCADE;
ALTER TABLE public.asso_terre ADD CONSTRAINT mect_fk FOREIGN KEY (id_mect)
REFERENCES public.mect (id_mect) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: bassin_fk | type: CONSTRAINT --
-- ALTER TABLE public.asso_terre DROP CONSTRAINT IF EXISTS bassin_fk CASCADE;
ALTER TABLE public.asso_terre ADD CONSTRAINT bassin_fk FOREIGN KEY (id_bassin)
REFERENCES public.bassin (id_bassin) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.zonage | type: TABLE --
-- DROP TABLE IF EXISTS public.zonage CASCADE;
CREATE TABLE public.zonage (
	id_zonage serial NOT NULL,
	version character varying,
	date date NOT NULL,
	CONSTRAINT zonage_pk PRIMARY KEY (id_zonage)

);
-- ddl-end --
-- ALTER TABLE public.zonage OWNER TO postgres;
-- ddl-end --

-- object: zonage_fk | type: CONSTRAINT --
-- ALTER TABLE public.asso_terre DROP CONSTRAINT IF EXISTS zonage_fk CASCADE;
ALTER TABLE public.asso_terre ADD CONSTRAINT zonage_fk FOREIGN KEY (id_zonage)
REFERENCES public.zonage (id_zonage) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.sources | type: TABLE --
-- DROP TABLE IF EXISTS public.sources CASCADE;
CREATE TABLE public.sources (
	id_source serial NOT NULL,
	nom_source character varying NOT NULL,
	lien character varying,
	contact character varying,
	commentaire character varying,
	CONSTRAINT sources_pk PRIMARY KEY (id_source)

);
-- ddl-end --
-- ALTER TABLE public.sources OWNER TO postgres;
-- ddl-end --

-- object: public.param_terrestre | type: TABLE --
-- DROP TABLE IF EXISTS public.param_terrestre CASCADE;
CREATE TABLE public.param_terrestre (
	id_param serial NOT NULL,
	nom_param character varying NOT NULL,
	unite character varying NOT NULL,
	famille character varying NOT NULL,
	methode_calc character varying NOT NULL,
	id_source integer NOT NULL,
	CONSTRAINT param_terrestre_pk PRIMARY KEY (id_param,id_source)

);
-- ddl-end --
-- ALTER TABLE public.param_terrestre OWNER TO postgres;
-- ddl-end --

-- object: sources_fk | type: CONSTRAINT --
-- ALTER TABLE public.param_terrestre DROP CONSTRAINT IF EXISTS sources_fk CASCADE;
ALTER TABLE public.param_terrestre ADD CONSTRAINT sources_fk FOREIGN KEY (id_source)
REFERENCES public.sources (id_source) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.pression_terrestre | type: TABLE --
-- DROP TABLE IF EXISTS public.pression_terrestre CASCADE;
CREATE TABLE public.pression_terrestre (
	id_bassin integer NOT NULL,
	id_param integer NOT NULL,
	valeur double precision NOT NULL,
	date_inf date NOT NULL,
	date_sup date NOT NULL,
	validite character varying NOT NULL,
	id_source integer NOT NULL,
	CONSTRAINT pression_terrestre_pk PRIMARY KEY (date_inf,date_sup,id_bassin,id_param,id_source)

);
-- ddl-end --
-- ALTER TABLE public.pression_terrestre OWNER TO postgres;
-- ddl-end --

-- object: public.param_potamologique | type: TABLE --
-- DROP TABLE IF EXISTS public.param_potamologique CASCADE;
CREATE TABLE public.param_potamologique (
	id_param serial NOT NULL,
	nom_param character varying NOT NULL,
	unite character varying NOT NULL,
	famille character varying NOT NULL,
	methode_calc character varying NOT NULL,
	id_source integer NOT NULL,
	CONSTRAINT param_potamologique_pk PRIMARY KEY (id_param,id_source)

);
-- ddl-end --
-- ALTER TABLE public.param_potamologique OWNER TO postgres;
-- ddl-end --

-- object: sources_fk | type: CONSTRAINT --
-- ALTER TABLE public.param_potamologique DROP CONSTRAINT IF EXISTS sources_fk CASCADE;
ALTER TABLE public.param_potamologique ADD CONSTRAINT sources_fk FOREIGN KEY (id_source)
REFERENCES public.sources (id_source) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.pression_potamologique | type: TABLE --
-- DROP TABLE IF EXISTS public.pression_potamologique CASCADE;
CREATE TABLE public.pression_potamologique (
	id_bassin integer NOT NULL,
	id_param integer NOT NULL,
	valeur double precision NOT NULL,
	date_inf date NOT NULL,
	date_sup date NOT NULL,
	validite character varying NOT NULL,
	id_source integer NOT NULL,
	CONSTRAINT pression_potamologique_pk PRIMARY KEY (date_inf,date_sup,id_bassin,id_param,id_source)

);
-- ddl-end --
-- ALTER TABLE public.pression_potamologique OWNER TO postgres;
-- ddl-end --

-- object: public.param_littoral | type: TABLE --
-- DROP TABLE IF EXISTS public.param_littoral CASCADE;
CREATE TABLE public.param_littoral (
	id_param serial NOT NULL,
	nom_param character varying NOT NULL,
	unite character varying NOT NULL,
	famille character varying NOT NULL,
	methode_calc character varying NOT NULL,
	id_source integer NOT NULL,
	CONSTRAINT param_littoral_pk PRIMARY KEY (id_param,id_source)

);
-- ddl-end --
-- ALTER TABLE public.param_littoral OWNER TO postgres;
-- ddl-end --

-- object: sources_fk | type: CONSTRAINT --
-- ALTER TABLE public.param_littoral DROP CONSTRAINT IF EXISTS sources_fk CASCADE;
ALTER TABLE public.param_littoral ADD CONSTRAINT sources_fk FOREIGN KEY (id_source)
REFERENCES public.sources (id_source) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.pression_littoral | type: TABLE --
-- DROP TABLE IF EXISTS public.pression_littoral CASCADE;
CREATE TABLE public.pression_littoral (
	id_mect integer NOT NULL,
	id_param integer NOT NULL,
	valeur double precision NOT NULL,
	date_inf date NOT NULL,
	date_sup date NOT NULL,
	validite character varying NOT NULL,
	id_source integer NOT NULL,
	CONSTRAINT pression_littoral_pk PRIMARY KEY (date_inf,date_sup,id_mect,id_param,id_source)

);
-- ddl-end --
-- ALTER TABLE public.pression_littoral OWNER TO postgres;
-- ddl-end --

-- object: public.param_indicateur_littoral | type: TABLE --
-- DROP TABLE IF EXISTS public.param_indicateur_littoral CASCADE;
CREATE TABLE public.param_indicateur_littoral (
	id_param serial NOT NULL,
	nom_param character varying NOT NULL,
	unite character varying NOT NULL,
	famille character varying NOT NULL,
	methode_calc character varying NOT NULL,
	id_source integer NOT NULL,
	CONSTRAINT param_indicateur_littoral_pk PRIMARY KEY (id_param,id_source)

);
-- ddl-end --
-- ALTER TABLE public.param_indicateur_littoral OWNER TO postgres;
-- ddl-end --

-- object: sources_fk | type: CONSTRAINT --
-- ALTER TABLE public.param_indicateur_littoral DROP CONSTRAINT IF EXISTS sources_fk CASCADE;
ALTER TABLE public.param_indicateur_littoral ADD CONSTRAINT sources_fk FOREIGN KEY (id_source)
REFERENCES public.sources (id_source) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.indicateur_littoral | type: TABLE --
-- DROP TABLE IF EXISTS public.indicateur_littoral CASCADE;
CREATE TABLE public.indicateur_littoral (
	id_mect integer NOT NULL,
	id_param integer NOT NULL,
	valeur double precision NOT NULL,
	date_inf date NOT NULL,
	date_sup date NOT NULL,
	validite character varying NOT NULL,
	id_source integer NOT NULL,
	CONSTRAINT indicateur_littoral_pk PRIMARY KEY (date_inf,date_sup,id_mect,id_param,id_source)

);
-- ddl-end --
-- ALTER TABLE public.indicateur_littoral OWNER TO postgres;
-- ddl-end --

-- object: public.asso_mect | type: TABLE --
-- DROP TABLE IF EXISTS public.asso_mect CASCADE;
CREATE TABLE public.asso_mect (
	id_mect integer NOT NULL,
	id_mect1 integer NOT NULL,
	id_zonage integer NOT NULL,
	connexion character varying NOT NULL,
	CONSTRAINT asso_mect_pk PRIMARY KEY (id_mect,id_zonage,id_mect1)

);
-- ddl-end --
-- ALTER TABLE public.asso_mect OWNER TO postgres;
-- ddl-end --

-- object: mect_fk | type: CONSTRAINT --
-- ALTER TABLE public.asso_mect DROP CONSTRAINT IF EXISTS mect_fk CASCADE;
ALTER TABLE public.asso_mect ADD CONSTRAINT mect_fk FOREIGN KEY (id_mect)
REFERENCES public.mect (id_mect) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: zonage_fk | type: CONSTRAINT --
-- ALTER TABLE public.asso_mect DROP CONSTRAINT IF EXISTS zonage_fk CASCADE;
ALTER TABLE public.asso_mect ADD CONSTRAINT zonage_fk FOREIGN KEY (id_zonage)
REFERENCES public.zonage (id_zonage) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: mect_fk1 | type: CONSTRAINT --
-- ALTER TABLE public.asso_mect DROP CONSTRAINT IF EXISTS mect_fk1 CASCADE;
ALTER TABLE public.asso_mect ADD CONSTRAINT mect_fk1 FOREIGN KEY (id_mect1)
REFERENCES public.mect (id_mect) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.bonus | type: TABLE --
-- DROP TABLE IF EXISTS public.bonus CASCADE;
CREATE TABLE public.bonus (
	id_objet serial NOT NULL,
	id_mect integer NOT NULL,
	code_objet varchar,
	type_objet varchar,
	commentaire varchar,
	"geom_point_L93" geometry(POINT),
	"geom_ligne_L93" geometry(MULTILINESTRING),
	"geom_polygone_L93" geometry(MULTIPOLYGON),
	id_bonus_source integer NOT NULL,
	version varchar NOT NULL,
	CONSTRAINT bonus_pk PRIMARY KEY (id_objet,id_bonus_source,version,id_mect)

);
-- ddl-end --
-- ALTER TABLE public.bonus OWNER TO postgres;
-- ddl-end --

-- object: public.bonus_source | type: TABLE --
-- DROP TABLE IF EXISTS public.bonus_source CASCADE;
CREATE TABLE public.bonus_source (
	id_bonus_source serial NOT NULL,
	nom_source varchar NOT NULL,
	version varchar NOT NULL,
	lien varchar,
	commentaire varchar,
	CONSTRAINT bonus_source_pk PRIMARY KEY (id_bonus_source,version)

);
-- ddl-end --
-- ALTER TABLE public.bonus_source OWNER TO postgres;
-- ddl-end --

-- object: bonus_source_fk | type: CONSTRAINT --
-- ALTER TABLE public.bonus DROP CONSTRAINT IF EXISTS bonus_source_fk CASCADE;
ALTER TABLE public.bonus ADD CONSTRAINT bonus_source_fk FOREIGN KEY (id_bonus_source,version)
REFERENCES public.bonus_source (id_bonus_source,version) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: mect_fk | type: CONSTRAINT --
-- ALTER TABLE public.bonus DROP CONSTRAINT IF EXISTS mect_fk CASCADE;
ALTER TABLE public.bonus ADD CONSTRAINT mect_fk FOREIGN KEY (id_mect)
REFERENCES public.mect (id_mect) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: bassin_fk | type: CONSTRAINT --
-- ALTER TABLE public.pression_terrestre DROP CONSTRAINT IF EXISTS bassin_fk CASCADE;
ALTER TABLE public.pression_terrestre ADD CONSTRAINT bassin_fk FOREIGN KEY (id_bassin)
REFERENCES public.bassin (id_bassin) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: param_terrestre_fk | type: CONSTRAINT --
-- ALTER TABLE public.pression_terrestre DROP CONSTRAINT IF EXISTS param_terrestre_fk CASCADE;
ALTER TABLE public.pression_terrestre ADD CONSTRAINT param_terrestre_fk FOREIGN KEY (id_param,id_source)
REFERENCES public.param_terrestre (id_param,id_source) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: mect_fk | type: CONSTRAINT --
-- ALTER TABLE public.pression_littoral DROP CONSTRAINT IF EXISTS mect_fk CASCADE;
ALTER TABLE public.pression_littoral ADD CONSTRAINT mect_fk FOREIGN KEY (id_mect)
REFERENCES public.mect (id_mect) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: param_littoral_fk | type: CONSTRAINT --
-- ALTER TABLE public.pression_littoral DROP CONSTRAINT IF EXISTS param_littoral_fk CASCADE;
ALTER TABLE public.pression_littoral ADD CONSTRAINT param_littoral_fk FOREIGN KEY (id_param,id_source)
REFERENCES public.param_littoral (id_param,id_source) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: bassin_fk | type: CONSTRAINT --
-- ALTER TABLE public.pression_potamologique DROP CONSTRAINT IF EXISTS bassin_fk CASCADE;
ALTER TABLE public.pression_potamologique ADD CONSTRAINT bassin_fk FOREIGN KEY (id_bassin)
REFERENCES public.bassin (id_bassin) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: param_potamologique_fk | type: CONSTRAINT --
-- ALTER TABLE public.pression_potamologique DROP CONSTRAINT IF EXISTS param_potamologique_fk CASCADE;
ALTER TABLE public.pression_potamologique ADD CONSTRAINT param_potamologique_fk FOREIGN KEY (id_param,id_source)
REFERENCES public.param_potamologique (id_param,id_source) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: mect_fk | type: CONSTRAINT --
-- ALTER TABLE public.indicateur_littoral DROP CONSTRAINT IF EXISTS mect_fk CASCADE;
ALTER TABLE public.indicateur_littoral ADD CONSTRAINT mect_fk FOREIGN KEY (id_mect)
REFERENCES public.mect (id_mect) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: param_indicateur_littoral_fk | type: CONSTRAINT --
-- ALTER TABLE public.indicateur_littoral DROP CONSTRAINT IF EXISTS param_indicateur_littoral_fk CASCADE;
ALTER TABLE public.indicateur_littoral ADD CONSTRAINT param_indicateur_littoral_fk FOREIGN KEY (id_param,id_source)
REFERENCES public.param_indicateur_littoral (id_param,id_source) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --


